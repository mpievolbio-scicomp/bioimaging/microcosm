#!/usr/bin/env python
# coding: utf-8

# In[1]:


from glob import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
import matplotlib
from functools import reduce

from skimage.morphology import disk
from skimage.feature import blob_log


import trackpy

### import code from src folder
import sys
import os
sys.path.append('../../src')
import viz
import log_blobs_detector 
import traj_descriptors as td

import warnings
warnings.filterwarnings('ignore')


# In[4]:


def find_path(df,file_id,cut):
    select_df=df[(df["file_id"]==file_id) & (df["cut"]==cut)]
    return select_df.iloc[0].path,select_df.iloc[0].track_stop

def find_blobs(df,file_id,cut,track_stop):
    select=df[(df["file_id"]==file_id) & (df["cut"]==cut) & ((df["frame"]<track_stop))]
    return select

def viz_trajectories(images,tracks,stop_frame,tracks_folder):
    from skimage.filters import gaussian
    for frame in range(stop_frame):
        fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(20,20))
        # show image
        img=gaussian(images[frame,:,:],2)
        ax.imshow(img,cmap='gray',vmin=np.quantile(img,0.1),vmax=np.quantile(img,0.95))
        ax.axis('off')
    
        # plot blobs
        result=tracks[tracks['frame']==frame]
        viz.blobs_overlay(result,frame,ax,color='red')
    
        # viz tracks
        tracks_frame=tracks[tracks['frame']<=frame]
        for id,group in tracks_frame.groupby("particle"):
            ax.plot(group['col'],group['row'],lw=2,color='cyan')
            
        
        fig.savefig(tracks_folder+"/im_traj_"+str(frame)+".png",bbox_inches='tight')


# ## Track blobs

# In[ ]:


## =========================== ##
## Read data
## =========================== ##

File_catalog=pd.read_csv("../../data/dt_300/File_catalog_frame.csv")
FileList=glob('../../data/dt_300/Detected*.csv')
Blobs=pd.DataFrame()

for file_name in FileList:
    Data=pd.read_csv(file_name)
    Blobs=pd.concat([Blobs,Data])
    
for idx in File_catalog.index:
    file_id=File_catalog.loc[idx].file_id
    cut=File_catalog.loc[idx].cut
    track_stop=File_catalog.loc[idx].track_stop
    file_path=File_catalog.loc[idx].path
    print(file_id,cut,track_stop,file_path)
    
    # work with blobs
    blobs_select=find_blobs(Blobs,file_id,cut,track_stop)
    blobs_select=blobs_select[blobs_select["contrast"]>0.05]
    
    # tracking
    blobs_select=blobs_select.rename(columns={"col":"x","row":"y"})
    tracks = trackpy.link_df(blobs_select,search_range=25)
    # filter out trajectories less than 3 frames
    clean_tracks = trackpy.filter_stubs(tracks,threshold=3)
    # rename back to my notation
    clean_tracks=clean_tracks.rename(columns={"x":"col","y":"row"})
    print("file_id={} cut={} tracks={}".format(file_id,cut,clean_tracks.groupby(["particle"]).ngroups))
    clean_tracks.to_csv(os.path.split(file_path)[0]+"/tracks_overlay_id_{}_cut_{}.csv".format(file_id,cut))
    
    # save overlays
    tracks_folder=os.path.join(os.path.split(file_path)[0],"tracks_overlay_id_{}_cut_{}".format(file_id,cut))
    os.mkdir(tracks_folder)
    images=io.imread(file_path)
    viz_trajectories(images,clean_tracks,track_stop,tracks_folder)


# In[ ]:




