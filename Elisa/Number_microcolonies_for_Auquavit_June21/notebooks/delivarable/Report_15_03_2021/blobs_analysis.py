#!/usr/bin/env python
# coding: utf-8

# In[64]:


from glob import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
import matplotlib


from skimage.morphology import disk
from skimage.feature import blob_log
from skimage.filters import gaussian

### import code from src folder
import sys
import os
sys.path.append('../../src')
import viz
import log_blobs_detector 
import traj_descriptors as td

import warnings
warnings.filterwarnings('ignore')

import seaborn as sns
sns.set(font_scale=1.5)


# In[65]:


FileList=glob('../../data/dt_300/Detected*.csv')
Blobs=pd.DataFrame()

for file_name in FileList:
    Data=pd.read_csv(file_name)
    Blobs=pd.concat([Blobs,Data])


# In[26]:


Blobs.shape


# ## Number of microcolonies vs time

# In[89]:


def blobs_counter(df,cut):
    Count=df.groupby(['frame']).apply(lambda x: len(x['radius']))
    A=Count.reset_index('frame')
    A=A.rename(columns={A.columns[1]:"Count"})
    A['Count_correct']=A['Count']-np.min(A['Count'])
    return A

df_Counter=pd.DataFrame()
for id, group in Blobs.groupby("file_id"):
    for (cut,idd) in group.groupby('cut'):
        print(id,cut)
        df_counter=blobs_counter(idd,cut)
        df_counter["file_id"]=id
        df_counter["cut"]=cut
        df_Counter=pd.concat([df_Counter,df_counter])


# In[90]:


df_Counter


# In[93]:


fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(15,5))
axx=ax.ravel()
counter=0

for id,group in df_Counter.groupby("file_id"):
    for cut,group_cut in group.groupby("cut"):
        axx[counter].plot(group_cut["frame"],group_cut["Count"],label=cut)
        axx[counter].legend()
        axx[counter].set_xlabel("frame")
        axx[counter].set_ylabel("# microcolonies")
        axx[counter].set_title("{}".format(id))
    counter=counter+1


    


#  #### corrected
#    
#     
#     

# In[94]:


fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(15,5))
axx=ax.ravel()
counter=0

for id,group in df_Counter.groupby("file_id"):
    for cut,group_cut in group.groupby("cut"):
        axx[counter].plot(group_cut["frame"],group_cut["Count_correct"],label=cut)
        axx[counter].legend()
        axx[counter].set_xlabel("frame")
        axx[counter].set_ylabel("# microcolonies")
        axx[counter].set_title("{}".format(id))
    counter=counter+1


    


# ## Substract initial level

# In[79]:


fig,axx=plt.subplots(nrows=1, ncols=1,figsize=(8,8))
sns.lineplot(data=df_sum_counter,x='frame',y='Count_correct',hue='file_id',style="cut",palette="tab10",ax=axx)


# ## 2D Histogrammes
# 

# In[80]:


def plot_2d_histogramme(Data,attr,scale_label,ax):
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    
    # data
    x = Data.frame
    y = Data[attr]
    
    # Big bins
    sns.set(font_scale=1.5)
    
    pl=ax.hist2d(x, y, bins=[len(Data.frame.unique()),9], cmap=plt.cm.jet);
    ax.set_xlabel("frame")
    ax.set_ylabel(attr+"  "+scale_label)
    ax.set_title(attr)
    ax.grid(False)
    cbar=fig.colorbar(pl[3], ax=ax)
    cbar.set_label("counts ")
    #fig.savefig(path+attr+"_2dhist.png",bbox_inches=None)
    


# In[101]:


sns.set(font_scale=4)
for id, group in Blobs.groupby("file_id"):
    fig,ax =plt.subplots(nrows=1, ncols=4,figsize=(50,16))
    counter=0
    for (cut,idd) in group.groupby('cut'):
        print(id,cut)
        plot_2d_histogramme(idd,'radius','px',ax[counter])
        ax[counter].set_title("file_id={}, cut={}".format(id,cut))
        counter=counter+1


# In[ ]:




