#!/usr/bin/env python
# coding: utf-8

# In[65]:


import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import skimage
import skimage.io as io


# In[67]:


FileList=glob('../../data/dt_300/*.tiff')
FileList


# In[68]:


for file_name in FileList:
    file_name_prefix=file_name.split('/')[-1].split(".tiff")[0]
    
    img=io.imread(file_name)
    print(img.shape)
    chanel_bright=img[:,0,:,:] # Bright Field
    chanel_dark=img[:,1,:,:]   # Dark Field
    
    io.imsave("../../data/dt_300/"+file_name_prefix+"_Dark.tif",chanel_dark,check_contrast=False,plugin='tifffile')

