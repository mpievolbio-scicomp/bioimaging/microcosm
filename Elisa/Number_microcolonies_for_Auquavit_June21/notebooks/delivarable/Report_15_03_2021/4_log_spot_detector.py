#!/usr/bin/env python
# coding: utf-8

# In[36]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
import matplotlib


from skimage.morphology import disk
from skimage.feature import blob_log
from skimage.filters import gaussian

### import code from src folder
import sys
import os
sys.path.append('../../src')
import viz
import log_blobs_detector 
import traj_descriptors as td

import warnings
warnings.filterwarnings('ignore')


# In[37]:


def detect_blobs(img):
    im=gaussian(img,sigma=2)
    detected_blobs=log_blobs_detector.detect_blobs_log_detector(im,2,8)
    return detected_blobs

def list_df_to_DataFrame(list_df):
    for n,rd in enumerate(list_df):
        rd['frame']=n
    df = pd.concat(list_df)
    return df


# ## Detect blobs across data set

# In[38]:


import multiprocess
number_of_cpus = multiprocess.cpu_count(); number_of_cpus
pool = multiprocess.Pool(processes=number_of_cpus)


# In[40]:


File_catalog=pd.read_csv("../../data/dt_300/File_catalog_frame.csv")

for id,group in File_catalog.groupby("file_id"):
    blobs_result_full_image=pd.DataFrame()
    for idx in group.index:
        print(group.loc[idx].file_id,group.loc[idx].cut)
        images=io.imread(group.loc[idx].path)
        img_list=[images[i] for i in range(group.loc[idx].frame_stop)]
        blobs_result= pool.map(detect_blobs,img_list)

        # combine list to one DataFrame
        blobs_result=list_df_to_DataFrame(blobs_result)
        blobs_result['file_id']=group.loc[idx].file_id
        blobs_result['cut']=group.loc[idx].cut
        blobs_result_full_image=pd.concat([blobs_result_full_image,blobs_result])
    blobs_result_full_image.to_csv("../../data/dt_300/Detected_blobs_{}".format(id)+'.csv')   
        


# In[ ]:




