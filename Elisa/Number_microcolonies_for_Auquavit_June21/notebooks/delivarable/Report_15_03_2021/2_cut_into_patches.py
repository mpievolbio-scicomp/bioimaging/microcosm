#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import skimage
import skimage.io as io


# ## Read data

# In[5]:


FileList=glob('../../data/dt_300/*Dark.tif')
FileList
images=io.imread(FileList[0])


# ## Create selection

# In[7]:


(frames,nrows,ncols)=images.shape
Center_row=np.int(nrows/2)
Center_col=np.int(ncols/2)

print(Center_row,Center_col)

d=700
Rectangle_1=[Center_col-d,Center_row-d,d,d]
Rectangle_2=[Center_col,Center_row-d,d,d]
Rectangle_3=[Center_col-d,Center_row,d,d]
Rectangle_4=[Center_col,Center_row,d,d]

Rectangle_list=[Rectangle_1,Rectangle_2,Rectangle_3,Rectangle_4]


# ## Viz on the images

# In[14]:


def show_plane(img,Rectangle_list,cmap="gray", title=None):
    from matplotlib.patches import Rectangle
    fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(20,20))
    ax.imshow(img, cmap=cmap,vmin=np.quantile(img,0.10),vmax=np.quantile(img,0.90))
    
    for (rect,color) in zip(Rectangle_list,['red','cyan','blue','yellow']):
        draw_rect =Rectangle((rect[0],rect[1]),rect[2],rect[3],linewidth=5,edgecolor=color,facecolor='none')
        ax.add_patch(draw_rect)
    ax.axis("off")
    if title:ax.set_title(title)


# In[9]:


for file_name in FileList:
    file_name_prefix=file_name.split('/')[-1].split(".tiff")[0]
    
    images=io.imread(file_name)
    show_plane(images[10],Rectangle_list,cmap="gray", title=file_name_prefix)
    
    #io.imsave("../../data/dt_300/"+file_name_prefix+"_Dark.tif",chanel_dark,check_contrast=False,plugin='tifffile')


# ## Cut into patches

# In[24]:


def cut_into_patches(images,Rectangle_list,title):
    fig,ax=plt.subplots(nrows=1,ncols=4,figsize=(20,20))
    

    for (rec,counter) in zip(Rectangle_list,[1,2,3,4]):
        col,row,width,hight=rec
        roi_images=images[:,row:row+hight,col:col+width]
        
        ax[counter-1].imshow(roi_images[10], cmap="gray",vmin=np.quantile(roi_images[10],0.10),vmax=np.quantile(roi_images[10],0.90))
        ax[counter-1].set_title(str(counter))
        io.imsave("../../data/dt_300/"+title+"_cut_"+str(counter)+'.tiff',roi_images,check_contrast=False,plugin='tifffile')
   


# In[25]:


for file_name in FileList:
    file_name_prefix=file_name.split('/')[-1].split(".tif")[0]
    
    images=io.imread(file_name)
    
    show_plane(images[10],Rectangle_list,cmap="gray", title=file_name_prefix)
    cut_into_patches(images,Rectangle_list,file_name_prefix)
    

