#!/usr/bin/env python
# coding: utf-8

# In[20]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
import matplotlib


from skimage.morphology import disk
from skimage.feature import blob_log
from skimage.filters import gaussian

### import code from src folder
import sys
import os
sys.path.append('../../src')
import viz
import log_blobs_detector 
import traj_descriptors as td

import warnings
warnings.filterwarnings('ignore')


# In[21]:


File_catalog=pd.read_csv("../../data/dt_300/File_catalog_frame.csv")

def find_path(df,file_id,cut):
    select_df=df[(df["file_id"]==file_id) & (df["cut"]==cut)]
    return select_df.iloc[0].path

def find_blobs(df,file_id,cut,frame):
    select=df[(df["file_id"]==file_id) & (df["cut"]==cut) & (df["frame"]==frame)]
    return select


# In[22]:


File_catalog


# In[23]:


file_id=43118
cut=3
frame=60

# select file
file=find_path(File_catalog,file_id,cut)
images=io.imread(file)

# read corresponding blobs
Blobs=pd.read_csv("../../data/dt_300/Detected_blobs_{}.csv".format(file_id))
Blobs_select=find_blobs(Blobs,file_id,cut,frame)


# visualize blobs
fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(20,20)) # visualize blobs
img=gaussian(images[frame],2)
ax.imshow(img,vmin=np.quantile(img,0.1),vmax=np.quantile(img,0.98),cmap='gray')
viz.blobs_overlay(Blobs_select,frame,ax,color='red')
Blobs_accept=Blobs_select[Blobs_select["contrast"]>=0.06]
viz.blobs_overlay(Blobs_accept,frame,ax,color='cyan')
fig.savefig('../../data/dt_300/'+"/Test_{}.tif".format(frame),bbox_inches='tight')


# In[ ]:


for id,group in File_catalog.groupby("file_id"):
    for idx in group.index:
        path_to_save='../../data/dt_300/{}_{}'.format(group.loc[idx].file_id,group.loc[idx].cut)
        os.mkdir(path_to_save)
        
        # get parameters
        file=group.loc[idx].path
        stop_frame=group.loc[idx].frame_stop
        file_id=group.loc[idx].file_id
        cut=group.loc[idx].cut
        
        
        images=io.imread(file)
        # read corresponding blobs
        Blobs=pd.read_csv("../../data/dt_300/Detected_blobs_{}.csv".format(group.loc[idx].file_id))
        
        for frame in range(stop_frame):
            Blobs_select=find_blobs(Blobs,file_id,cut,frame)
            
            fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(20,20)) # visualize blobs
            img=gaussian(images[frame],2)
            ax.imshow(img,vmin=np.quantile(img,0.1),vmax=np.quantile(img,0.98),cmap='gray')
            viz.blobs_overlay(Blobs_select,frame,ax,color='red')
            Blobs_accept=Blobs_select[Blobs_select["contrast"]>=0.06]
            viz.blobs_overlay(Blobs_accept,frame,ax,color='cyan')
            fig.savefig(path_to_save+"/img_{}.tif".format(frame),bbox_inches='tight')
        


# In[ ]:




