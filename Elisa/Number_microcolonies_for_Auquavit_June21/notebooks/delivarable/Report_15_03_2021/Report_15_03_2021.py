#!/usr/bin/env python
# coding: utf-8

# In[37]:


from glob import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
import matplotlib
from functools import reduce

from skimage.morphology import disk
from skimage.feature import blob_log


import trackpy

### import code from src folder
import sys
import os
sys.path.append('../../src')
import viz
import log_blobs_detector 
import traj_descriptors as td
import data_viz

import warnings
warnings.filterwarnings('ignore')


# ## Dataset description
# 
# This report presents the analysis of three image sequences with the minimal time period between frames - 300 seconds.
# 
# * **OmeroID_43113**<br>
#   Name="2021Feb16_6wellplate_lid_8ml_kb_aliquot_glyc_WT-01.czi" <br>
#   SizeT="138"<br>
# * **OmeroID_43118**<br>
#   Name="2021Feb17_6wellplate_lid_8ml_kb_aliquot_glyc_WT-04.czi"<br>
#   SizeT="128"<br>
# * **OmeroID_43048**<br>
#   Name="2021Feb15_6wellplate_lid_8ml_kb_aliquot_glyc_WT-01.czi" <br>
#   SizeT="49"<br>
# 
# 
# 
# 
# **Physical units**: <br>
# 1. Pixel size 12.97 µm
# 2. Image size 2752x2208 pixels
# 3. Frame taken every 5 minutes (300 s)
# 
# 
# 
# 
# 

# ## Frame processing: four patches
# 
# We analyze images for four selected regions of interest. see examples in the figure box below. Thus, we dropped the outer parts of the image, which contain very bright reflections of the container walls. The selected regions of interest serve as an internal control for image analysis pipeline. 
# 
# Locations of each patch was the same in all frames across the dataset. While each patch was processed independently, the results from all patches belonging to the same dataset are always presented together.

# In[45]:


FileList=glob('../../data/dt_300/*Dark.tif')
images=io.imread(FileList[0])

##================================
### physical units
##================================
dxx=12.97142879233052 #mkm
dt=5 #min

##================================
### create selection
##================================
(frames,nrows,ncols)=images.shape
Center_row=np.int(nrows/2)
Center_col=np.int(ncols/2)


d=700
Rectangle_1=[Center_col-d,Center_row-d,d,d]
Rectangle_2=[Center_col,Center_row-d,d,d]
Rectangle_3=[Center_col-d,Center_row,d,d]
Rectangle_4=[Center_col,Center_row,d,d]

Rectangle_list=[Rectangle_1,Rectangle_2,Rectangle_3,Rectangle_4]


##================================
### viz on the image
##================================
def show_plane(img,Rectangle_list,cmap="gray", title=None):
    """ show patches on the image frame    """
    from matplotlib.patches import Rectangle
    fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(20,20))
    ax.imshow(img, cmap=cmap,vmin=np.quantile(img,0.10),vmax=np.quantile(img,0.90))
    
    counter=1
    for (rect,color) in zip(Rectangle_list,['red','cyan','blue','yellow']):
        draw_rect =Rectangle((rect[0],rect[1]),rect[2],rect[3],linewidth=5,edgecolor=color,facecolor='none')
        ax.add_patch(draw_rect)
        ax.text(rect[0]+rect[2]/2,rect[1]+rect[3]/2,str(counter),fontsize=20,color='white')
        counter=counter+1
    ax.axis("off")
    if title:ax.set_title(title)

##================================
### viz patches
##================================        
def cut_into_patches(images,Rectangle_list,title):
    """ show patches separatedly   """
    fig,ax=plt.subplots(nrows=1,ncols=4,figsize=(20,20))
    
    for (rec,counter) in zip(Rectangle_list,[1,2,3,4]):
        col,row,width,hight=rec
        roi_images=images[:,row:row+hight,col:col+width]
        
        ax[counter-1].imshow(roi_images[10], cmap="gray",vmin=np.quantile(roi_images[10],0.10),vmax=np.quantile(roi_images[10],0.90))
        ax[counter-1].set_title(str(counter))
        ax[counter-1].axis("off")
        io.imsave("../../data/dt_300/"+title+"_cut_"+str(counter)+'.tiff',roi_images,check_contrast=False,plugin='tifffile')


        
##================================
### viz patches
##================================          
for file_name in FileList:
    file_name_prefix=file_name.split('/')[-1].split(".tif")[0]
    
    images=io.imread(file_name)
    
    show_plane(images[10],Rectangle_list,cmap="gray", title=file_name_prefix)
    cut_into_patches(images,Rectangle_list,file_name_prefix)        


# ## Log spot detector
# 
# Micro colonies are bright spots on dark in images. To detect the spots we used Laplacian of Gaussian (LoG) blob detector. We set parameters of blob detector to detect blobs in a range of radiuses from 25 to 100 micrometers. The main advantage of LoG blob detector is that it operates on derivatives of intensities and does not depend on the absolute values of intensities. Thus, LoG detector works well in the case of non-uniformly illuminated frames. The figure below demonstrates the example of blobs detection (marked by circles).
# 
# The LoG detector is sensitive and not every spot detected is actually a micro colony - some are results of the noise of the background, and some are artifacts. Hence, to remove false positive detections, we additionally introduced a filter by the contrast value. To calculate contrast we calculated median intensities within blob $I_{in}$ and a median intensity $I_{out}$ in circular layer around blob (R,2R).
# 
# $ contrast=\frac{(I_{in}-I_{out})}{(I_{in}+I_{out})}$
# 
# The filter allowed to pass these spots, which are significantly brighter than the area around them $contrast>0.05$. These spots, which brightness is similar to the surrounding area were discarded $contrast<0.05$.
# 
# The figure below demonstrates the example of spot detection and filtering on a single frame. Each circle indicates the found spot, its size corresponds to identified spot size, and the color of the circle shows whether a spot passed the filter (cyan) or was discarded as false positive (red). 

# In[ ]:


# def detect_blobs(img):
#     im=gaussian(img,sigma=2)
#     detected_blobs=log_blobs_detector.detect_blobs_log_detector(im,2,8)
#     return detected_blobs

# def list_df_to_DataFrame(list_df):
#     for n,rd in enumerate(list_df):
#         rd['frame']=n
#     df = pd.concat(list_df)
#     return df


# import multiprocess
# number_of_cpus = multiprocess.cpu_count(); number_of_cpus
# pool = multiprocess.Pool(processes=number_of_cpus)



# File_catalog=pd.read_csv("../../data/dt_300/File_catalog_frame.csv")

# for id,group in File_catalog.groupby("file_id"):
#     blobs_result_full_image=pd.DataFrame()
#     for idx in group.index:
#         print(group.loc[idx].file_id,group.loc[idx].cut)
#         images=io.imread(group.loc[idx].path)
#         img_list=[images[i] for i in range(group.loc[idx].frame_stop)]
#         blobs_result= pool.map(detect_blobs,img_list)

#         # combine list to one DataFrame
#         blobs_result=list_df_to_DataFrame(blobs_result)
#         blobs_result['file_id']=group.loc[idx].file_id
#         blobs_result['cut']=group.loc[idx].cut
#         blobs_result_full_image=pd.concat([blobs_result_full_image,blobs_result])
#     blobs_result_full_image.to_csv("../../data/dt_300/Detected_blobs_{}".format(id)+'.csv')   
        


# In[14]:


File_catalog=pd.read_csv("../../data/dt_300/File_catalog_frame.csv")

def find_path(df,file_id,cut):
    select_df=df[(df["file_id"]==file_id) & (df["cut"]==cut)]
    return select_df.iloc[0].path

def find_blobs(df,file_id,cut,frame):
    select=df[(df["file_id"]==file_id) & (df["cut"]==cut) & (df["frame"]==frame)]
    return select


CONTRAST_LEVEL=0.05
file_id=43118
cut=3
frame=60

# select file
file=find_path(File_catalog,file_id,cut)
images=io.imread(file)

# read corresponding blobs
Blobs=pd.read_csv("../../data/dt_300/Detected_blobs_{}.csv".format(file_id))
Blobs_select=find_blobs(Blobs,file_id,cut,frame)


# visualize blobs
fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(20,20)) # visualize blobs
img=gaussian(images[frame],2)
ax.imshow(img,vmin=np.quantile(img,0.1),vmax=np.quantile(img,0.98),cmap='gray')
viz.blobs_overlay(Blobs_select,frame,ax,color='red')
Blobs_accept=Blobs_select[Blobs_select["contrast"]>=CONTRAST_LEVEL]
viz.blobs_overlay(Blobs_accept,frame,ax,color='cyan')
#fig.savefig('../../data/dt_300/'+"/Test_{}.tif".format(frame),bbox_inches='tight')

ax.set_title("file_id={} cut={} frame={}".format(file_id,cut,frame));


# ## Spots analysis
# 
# ### Number of spots
# 
# We measured the dynamics of spots numbers in each dataset, see the figure below.
# 
# Each dataset shows that the number of spots grows with time. In the datasets 43113 and 43118, their growth rate accelerates with time. The same pattern is uniformly observed in each frame. However, in each dataset, bottom patches 3,4 feature a smaller number of spots than top patches 1,2. This difference might be due to the variation in lighting conditions between patches.
# 
# The dynamics of spots numbers in the dataset 43048 appears to be different from two other datasets. However, this might be just a result of the time delay of 43048. Starting from about 250 minutes, both 43113 and 43118 demonstrate very similar dynamics to the one found in 43048.

# In[29]:


FileList_Blobs=glob('../../data/dt_300/Detected*.csv')
Blobs=pd.DataFrame()

for file_name in FileList_Blobs:
    Data=pd.read_csv(file_name)
    Blobs=pd.concat([Blobs,Data])

    
Blobs=Blobs[Blobs["contrast"]>CONTRAST_LEVEL]


def blobs_counter(df,cut):
    Count=df.groupby(['frame']).apply(lambda x: len(x['radius']))
    A=Count.reset_index('frame')
    A=A.rename(columns={A.columns[1]:"Count"})
    A['Count_correct']=A['Count']-np.min(A['Count'])
    return A

df_Counter=pd.DataFrame()
for id, group in Blobs.groupby("file_id"):
    for (cut,idd) in group.groupby('cut'):
        #print(id,cut)
        df_counter=blobs_counter(idd,cut)
        df_counter["file_id"]=id
        df_counter["cut"]=cut
        df_Counter=pd.concat([df_Counter,df_counter])

        
fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(15,5),sharex=True)
axx=ax.ravel()
counter=0

for id,group in df_Counter.groupby("file_id"):
    for cut,group_cut in group.groupby("cut"):
        axx[counter].plot(group_cut["frame"]*dt,group_cut["Count"],label=cut)
        axx[counter].legend()
        axx[counter].set_xlabel("minutes")
        axx[counter].set_ylabel("# microcolonies")
        axx[counter].set_title("{}".format(id))
    counter=counter+1


    


# In[28]:


get_ipython().run_line_magic('pinfo', 'plt.subplots')


# In[31]:


fig,axx=plt.subplots(nrows=1, ncols=1,figsize=(8,8))
axx.set_title("Number of detected spots")
df_Counter['minutes']=df_Counter['frame']*dt
sns.lineplot(data=df_Counter,x='minutes',y='Count',hue='file_id',style="cut",palette="tab10",ax=axx);


# ### Dynamics of the spots size
# 
# The dynamics of the spots size distribution are shown below. Each vertical column in the heatmap corresponds to a single frame. The color of the pixel shows how many spots of the given size were found on the given frame. 
# 
# There are two classes of spots clearly visible on the heatmaps. The first class consists of smaller spots with sizes 50-60 mkm. Spots of this class are present in each dataset from the very beginning of the record. Their numbers remains mostly unchanged with a slight decline towards the end of the record. The second class contains larger spots - 60-80 mkm, which emerge at around 150-180 minutes in the datasets 43113, 43118. Their number rapidly grows yet their sizes remains mostly the same.

# In[36]:


def plot_2d_histogramme(Data,attr,dxx,scale_label,ax):
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    
    # data
    x = Data.minutes
    y = Data[attr]*dxx
    
    # Big bins
    sns.set(font_scale=1.5)
    
    pl=ax.hist2d(x, y, bins=[len(Data.frame.unique()),15], cmap=plt.cm.jet);
    ax.set_xlabel("minutes")
    ax.set_ylabel(attr+"  "+scale_label)
    ax.set_title(attr)
    ax.grid(False)
    cbar=fig.colorbar(pl[3], ax=ax)
    cbar.set_label("counts ")
    #fig.savefig(path+attr+"_2dhist.png",bbox_inches=None)

for id, group in Blobs.groupby("file_id"):
    fig,ax =plt.subplots(nrows=1, ncols=4,figsize=(27,6))
    counter=0
    for (cut,idd) in group.groupby('cut'):
        #print(id,cut)
        idd['minutes']=idd['frame']*dt
        plot_2d_histogramme(idd,'radius',dxx,'mkm',ax[counter])
        ax[counter].set_title("file_id={}, cut={}".format(id,cut))
        counter=counter+1


# ## Blobs tracking (work in progress)
# 
# For blobs tracing we used published and well described package **trackpy**, which is often used for colloidal particles tracking. The tracker connect particles that moved up to 20 pixels. To avoid wrong connections the tracker has a limitation to the maximum density of blobs. All found trajectories are shown on the figure below. Note that  second region of interest of the dataset 43048 has very hight density for trackpy to track.
# 
# We can see that some trajectories need to be filtered, for example for 43113 for 1 and 2 patch the lines are due to the scratches on the images.

# In[39]:


dxx=12.97 # mkm
dt=300 #s

FileList=glob('../../data/dt_300/tracks_overlay*.csv')
Tracks_Data=pd.DataFrame()

for file in FileList:
    tracks=pd.read_csv(file)
    Tracks_Data=pd.concat([Tracks_Data,tracks])
    
    
data_viz.plot_traj_overlay_replicas(Tracks_Data,1)


# In[41]:


#Tracks_Data["row"]=Tracks_Data["row"]*dxx
#Tracks_Data["col"]=Tracks_Data["col"]*dxx
#Tracks_Data["frame"]=Tracks_Data["frame"]*dt

grouped_tracks=Tracks_Data.groupby(['file_id', 'cut', 'particle'])
df1=grouped_tracks.apply(td.total_displacement).reset_index(name='TOTAL_DIST')
df2=grouped_tracks.apply(td.end_to_end_displacement).reset_index(name='END_TO_END')
df3=grouped_tracks.apply(td.max_distance_origin).reset_index(name='MAX_DIST_ORIG')
df4=grouped_tracks.apply(len).reset_index(name='TRAJ_LEN')
df5=grouped_tracks.apply(td.MSD).reset_index(name='MSD')

df=[df1,df2,df3,df4,df5]
TrajDescriptors = reduce(lambda  left,right: pd.merge(left,right,on=['file_id', 'cut', 'particle'],how='outer'), df)


# ###  Number of trajectories
# 
# A similar number of trajectories was identified on each patch, see the figure below.

# In[42]:


import seaborn as sns
sns.set(font_scale=1.2)
fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(7,5))
Stat_Traj_Number=TrajDescriptors.groupby(['file_id','cut']).apply(lambda X: np.size(np.unique(X["particle"]))).reset_index(name='N_TRAJ')
sns.barplot(x="file_id",y="N_TRAJ",hue="cut",palette="tab10",data=Stat_Traj_Number,ax=ax)
ax.set_title("Number trajectories per experiment")


# ### Global descriptors
# 
# We computed a number of descriptors for each trajectory. Their distributions are shown below.
# * "Length of the trajectory" is the number of frames at which the same object was detected.
# * "Total distance" is the combined length of all displacements of an object (the changes of the spot position between consecutive frames).
# * "End to end" is the physical distance between the first and the last recorded positions of an object.
# * "Maximal distance to origin" is the maximal physical distance of an object from its first recorded position.
# * "MSD" is the mean square displacement

# In[44]:


fig,axx=plt.subplots(nrows=2,ncols=3,figsize=(20,15))
ax=axx.ravel()
sns.violinplot(x="file_id",y="TRAJ_LEN",hue="cut",palette="tab10",showfliers = False,data=TrajDescriptors,ax=ax[0])
ax[0].set_title("Lengths of trajectories");

sns.violinplot(x="file_id",y='TOTAL_DIST',hue="cut",palette="tab10",showfliers = False,data=TrajDescriptors,ax=ax[1])
ax[1].set_title("total dist");

sns.violinplot(x="file_id",y='END_TO_END',hue="cut",palette="tab10",showfliers = False,data=TrajDescriptors,ax=ax[2])
ax[2].set_title("end to end");

sns.violinplot(x="file_id",y='MAX_DIST_ORIG',hue="cut",palette="tab10",showfliers = False,data=TrajDescriptors,ax=ax[3])
ax[3].set_title("max dist origin");

sns.violinplot(x="file_id",y='MSD',hue="cut",palette="tab10",showfliers = False,data=TrajDescriptors,ax=ax[4])
ax[4].set_title("MSD");


# In[ ]:




