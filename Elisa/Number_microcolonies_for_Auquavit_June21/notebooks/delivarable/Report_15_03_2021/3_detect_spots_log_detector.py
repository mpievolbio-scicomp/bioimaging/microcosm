#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import skimage
import skimage.io as io


# ## Create DataFrame to navigate files

# In[96]:


FileList=glob('../../data/dt_300/*cut*')
file_id=[i.split("ID")[1].split("_")[2].split('.')[0] for i in FileList]
cut=[i.split("ID")[1].split("_")[-1].split('.')[0] for i in FileList]
FileList

Files_df=pd.DataFrame({"path":FileList,"file_id":file_id,"cut":cut})


# In[98]:


Files_df.cut=Files_df.cut.astype('int')


# ## Calculate RMS contrast

# In[99]:


def calculate_contrast(images,ax,cut):
    (frame,row,col)=images.shape
    images_reshaped=images.reshape(frame,row*col)
    images_reshaped=(images_reshaped-np.min(images_reshaped))/(np.max(images_reshaped)-np.min(images_reshaped))
    contrast=np.std(images_reshaped,axis=1)
    ax.plot(contrast,label=cut)
    ax.legend()


# In[102]:


fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(15,5))
axx=ax.ravel()
counter=0


for id, group in Files_df.groupby("file_id"):
    group=group.sort_values(by='cut') 
    for idd in group.index:
        file_path=group.loc[idd].path
        images=io.imread(file_path)
        calculate_contrast(images,axx[counter],group.loc[idd].cut)
        axx[counter].set_title(group.loc[idd].file_id)
    counter=counter+1


# In[ ]:




