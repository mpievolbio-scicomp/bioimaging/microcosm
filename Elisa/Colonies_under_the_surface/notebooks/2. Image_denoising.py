#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import os


import skimage.io as io
from skimage import util
from skimage.filters import gaussian

import warnings
warnings.filterwarnings('ignore')
from glob import glob


# In[2]:


# viz function
def img_compare(img_dict):
    n_figures=len(img_dict)
    
    fig, ax = plt.subplots(1,n_figures,figsize=(5*n_figures, 5))
    counter=0
    for label in img_dict:
        img=img_dict[label]
        ax[counter].imshow(img[0:500,0:500])
        ax[counter].set_title(label)
        ax[counter].axis('off')
        counter=counter+1
        


# ## Image denoising
# 1. Small (1.5px) gauss kernel 
# 2. Substract background. Background estimated by large gauss kernel (150 px)
# 
# We saved intermediate images:
# * denoised -small gauss kernel
# * background - large gauss kernel
# * prepared -final denoised image

# In[3]:


def image_denoising(img):
    """
    Denoised img 
    Small (1.5px) gauss kernel 
    Substract background. Background estimated by large gauss kernel (150 px)
    """
    # denoised
    denoised=gaussian(img,1.5)
    # substract background
    background=gaussian(img,150)
    img_prepared=denoised-background
    return denoised,background,img_prepared

def images_list(file_name):
    """
    create list of images that used for multiprocessing
    """ 
    
    folder_to_save=os.path.split(file_name)[0]
    file_prefix=os.path.split(file_name)[1].split('.')[0]
    img_stack=io.imread(file_name)
    
    # make a list with images for multiprocessor
    img_list=[img_stack[i] for i in range(img_stack.shape[0])]
    return img_list,folder_to_save,file_prefix

def save_results_after_multiprocess(denoising_results,folder_to_save,file_prefix):
    """
    saved results of multiprocessing
    """
    n_frames=len(denoising_results)
    rows,cols=denoising_results[0][0].shape
    
    denoised=np.zeros((n_frames,rows,cols),dtype="float32")
    background=np.zeros((n_frames,rows,cols),dtype="float32")
    img_prepared=np.zeros((n_frames,rows,cols),dtype="float32")
    
    for frame in range(n_frames):
        denoised[frame],background[frame],img_prepared[frame]=denoising_results[frame]
        
    io.imsave(os.path.join(folder_to_save,file_prefix+"_denoised.tif"),denoised)
    io.imsave(os.path.join(folder_to_save,file_prefix+"_background.tif"),background)
    io.imsave(os.path.join(folder_to_save,file_prefix+"_prepared.tif"),img_prepared)


# In[4]:


file_list=glob("../data/*_Dark.tif")
file_list


# In[5]:


import multiprocess
number_of_cpus = multiprocess.cpu_count(); number_of_cpus
pool = multiprocess.Pool(processes=number_of_cpus)

for file_name in file_list:
    img_list,folder_to_save,file_prefix=images_list(file_name)
    denoising_results = pool.map(image_denoising,img_list)
    save_results_after_multiprocess(denoising_results,folder_to_save,file_prefix)


# In[ ]:




