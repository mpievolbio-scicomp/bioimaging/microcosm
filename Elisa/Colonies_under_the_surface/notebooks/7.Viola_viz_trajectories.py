#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import os
# import matplotlib.pyplot as plt
# import matplotlib
import seaborn as sns

import skimage.io as io
from skimage import util

from glob import glob
import trackpy

import ast

#from ipywidgets import interact, IntSlider,BoundedIntText
import ipywidgets as widgets
from IPython.display import display

get_ipython().run_line_magic('matplotlib', 'widget')

import matplotlib.pyplot as plt


# In[8]:


file_list=glob("../data/*_Dark_prepared.tif")
images_dict={}
for file_name in file_list:
    images_prepared=io.imread(file_name)
    file_prefix=os.path.split(file_name)[1].split('.')[0].split("_prepared")[0]
    
    binary_file=file_name.split(".tif")[0]+'_segmented'+'.tif'
    images_binary=io.imread(binary_file)
    tracks=pd.read_csv("../data/tracks_overlay_id_"+file_prefix+'.csv')
    tracks_big=pd.read_csv("../data/tracks_overlay_id_"+file_prefix+'_big.csv')
    img_dict={"img":images_prepared,"binary":images_binary,"tracks":tracks,"tracks_big":tracks_big}
    
    images_dict[file_prefix]=img_dict


# ## Viz trajectories

# In[3]:


class App:
    def __init__(self,images_dict):
        
        # define initial values
        self.images_dict = images_dict
        
        self.file_prefix='43125_Dark'
        self.images=self.images_dict[self.file_prefix]['binary']
        self.tracks=self.images_dict[self.file_prefix]['tracks_big']
        print(self.tracks.shape)
        self.max_frame=self.images.shape[0]-1
        
    
        
        # define widgets
        self._play = widgets.Play(value=0,min=0,max=self.max_frame,step=1,interval=200,description="Press play",disabled=False)
        self._slider = widgets.IntSlider(min=0,max=self.max_frame)
        widgets.jslink((self._play, 'value'), (self._slider, 'value'))
        self._select_experiment=widgets.Dropdown(value=self.file_prefix,
                                           placeholder='experiment OmeID',
                                           options=['43125_Dark', '43121_Dark', '43122_Dark', '43123_Dark'],
                                           description='Experiment:',
                                           disabled=False)
        self._plot_container = widgets.Output()
        
        # plot to update
        with self._plot_container:
            self.fig,self.ax = plt.subplots(constrained_layout=True,nrows=1,ncols=1)
            self.im = self.ax.imshow(self.images_dict[self.file_prefix]['binary'][0],cmap='gray')
            self.line, =self.ax.plot([0],[0],lw=0.5,color='red')
            self.ax.axis('off')
            
        self.container=widgets.VBox([
            self._select_experiment,
            widgets.HBox([self._play,self._slider]),
            self._plot_container])
        
        #self._update_app()
        #self.update_plot()
        
        # observe change
        #self._play.observe(self._on_change, names=['value'])
        self._slider.observe(self.update_plot, names='value')
        self._select_experiment.observe(self.update_file, 'value')
        

    def update_file(self,change):
        # reset file value
        self.file_prefix=change.new
        self.images=self.images_dict[self.file_prefix]['binary']
        self.tracks=self.images_dict[self.file_prefix]['tracks_big']
        self.max_frame=self.images.shape[0]
        self._slider.value=0
        self._slider.max=self.max_frame-1
        
        # restart plot
        self.ax.clear()
        self.im = self.ax.imshow(self.images[0])
        self.ax.axis('off')
        
        #self.im.set_data(self.images[0])
        self.fig.canvas.draw_idle()
  
    def update_plot(self,change):
        frame=change.new
        images=self.images_dict[self.file_prefix]['binary']
        self.im.set_data(images[frame])
        
        tracks_frame=self.tracks[self.tracks['frame']<=frame]
        
        for id,group in tracks_frame.groupby("particle"):
            self.ax.plot(group['x'],group['y'],lw=0.5,color='red')
            
        self.fig.canvas.draw_idle()
    
        
        


# In[9]:


app = App(images_dict)
app.container


# ## Viz last frame

# In[5]:


class VizLastFRame:
    def __init__(self,images_dict):
        
        # define initial values
        self.images_dict = images_dict
        self.file_prefix='43125_Dark'
        self.images=self.images_dict[self.file_prefix]['binary']
        self.tracks=self.images_dict[self.file_prefix]['tracks']
        self.max_frame=self.images.shape[0]-1
        
    
        
        # define widgets
        self._select_experiment=widgets.Dropdown(value=self.file_prefix,
                                           placeholder='experiment OmeID',
                                           options=['43125_Dark', '43121_Dark', '43122_Dark', '43123_Dark'],
                                           description='Experiment:',
                                           disabled=False)
        self._plot_container = widgets.Output()
        
        # plot to update
        with self._plot_container:
            self.fig,self.ax = plt.subplots(constrained_layout=True,nrows=1,ncols=1)
            self.im = self.ax.imshow(self.images_dict[self.file_prefix]['binary'][self.max_frame],cmap='gray')
            self.update_plot()
            self.ax.axis('off')
            
        self.container=widgets.VBox([
            self._select_experiment,
            self._plot_container])
        
        #self._update_app()
        #self.update_plot()
        
        # observe change
        self._select_experiment.observe(self.update_file, 'value')
        
        
    
    def update_file(self,change):
        # reset file value
        self.file_prefix=change.new
        self.images=self.images_dict[self.file_prefix]['binary']
        self.tracks=self.images_dict[self.file_prefix]['tracks']
        self.max_frame=self.images.shape[0]-1
        
        # restart plot
        self.ax.clear()
        self.im = self.ax.imshow(self.images[0])
        self.ax.axis('off')
        self.update_plot()
  
    def update_plot(self):
        images=self.images_dict[self.file_prefix]['binary']
        self.im.set_data(images[self.max_frame])
        tracks_frame=self.tracks[self.tracks['frame']<=self.max_frame]
        
        for id,group in tracks_frame.groupby("particle"):
            self.ax.plot(group['x'],group['y'],lw=0.5,color='red')
        self.fig.canvas.draw_idle()     


# In[6]:


app = VizLastFRame(images_dict)
app.container


# ## Close look at the objects

# In[7]:


class CloseUpObject:
    def __init__(self,images_dict):
        
        # define initial values
        self.images_dict = images_dict
        self.file_prefix='43125_Dark'
        self.particle=3
        
        # define widgets
        self._select_experiment=widgets.Dropdown(value=self.file_prefix,
                                           placeholder='experiment OmeID',
                                           options=['43125_Dark', '43121_Dark', '43122_Dark', '43123_Dark'],
                                           description='Experiment:',
                                           disabled=False)
        self._select_particle=widgets.IntText(value=self.particle,description='particle:',disabled=False)
        self._plot_container = widgets.Output()
        
        # activate trajectory
        self.traj_select([])
        
        # initialise plots
        with self._plot_container:
            self.fig,self.ax=plt.subplots(constrained_layout=True,nrows=1,ncols=3,figsize=(15,5))
            
            self.im_1=self.ax[0].imshow(self.traj_img[0],cmap='gray')
            self.ax[0].axis('off')
            
            self.im_2=self.ax[1].imshow(self.traj_seg[0],cmap='gray')
            self.ax[1].axis('off')
            
            self.line_1,=self.ax[2].plot(self.traj.frame,self.traj.orientation,'-o')
            self.ax[2].plot(self.traj.frame,self.traj.orientation,'-o')
            #self.ax[2].axvline(x=frame,color='red')
            self.ax[2].set_title("orientation")
            self.ax[2].set_xlabel("frame")
            self.ax[2].set_ylabel("area")
            
            #self.update_plot()
        self.container=widgets.VBox([
             self._select_experiment,
             self._select_particle,
             self._plot_container])
        
        # observe change
        self._select_experiment.observe(self.traj_select, 'value')
        self._select_particle.observe(self.traj_select, 'value')
        
    def update_file(self,change):
        
    def traj_select(self,_):
        self.file_prefix=self._select_experiment.value
        self.particle=self._select_particle.value
        self.images=self.images_dict[self.file_prefix]["img"]
        self.images_binary=self.images_dict[self.file_prefix]['binary']
        self.tracks=self.images_dict[self.file_prefix]['tracks']
        self.traj=self.tracks[self.tracks["particle"]==self.particle]
        
        
        min_row,min_col=np.min(self.traj.loc[:,'bbox-0':'bbox-1'])
        max_row,max_col=np.max(self.traj.loc[:,'bbox-2':'bbox-3'])

        # expand bounding box for better viz (min_row-delta,max_row+delta)
        delta=10
        min_row=np.max([min_row-delta,0])
        min_col=np.max([min_col-delta,0])
        max_row=np.min([max_row+delta,self.images[0].shape[0]])
        max_col=np.min([max_col+delta,self.images[0].shape[1]])
               
        self.traj_img=self.images[:,min_row:max_row,min_col:max_col]
        self.traj_seg=self.images_binary[:,min_row:max_row,min_col:max_col]
        
        # restart plot
        #self.ax.clear()
#         self.ax[1].clear()
#         self.ax[2].clear()
#         self.im = self.ax.imshow(self.images[0])
#         self.ax.axis('off')
        #self.plot_plane
      
    
#     def plot_plane(self):
        
#         # particle image
#         self.im.set_data(images[self.max_frame])
#         ax[0].imshow(traj_img[frame],cmap='gray')
#         ax[0].axis("off")

        

#         # particle image
#         ax[1].imshow(traj_seg[frame],cmap='gray')
#         ax[1].axis("off")

#         # area vs frame plot
#         ax[2].plot(traj.frame,traj.orientation,'-o')
#         ax[2].axvline(x=frame,color='red')
#         ax[2].set_title("orientation")
#         ax[2].set_xlabel("frame")
#         ax[2].set_ylabel("area")
    
        
    
#     def update_file(self,change):
#         # reset file value
#         self.file_prefix=change.new
#         self.images=self.images_dict[self.file_prefix]['"img"]
#         self.images_binary=self.images_dict[self.file_prefix]['binary']
#         self.tracks=self.images_dict[self.file_prefix]['tracks']
#         self.max_frame=self.images.shape[0]-1
        
#         # restart plot
#         self.ax.clear()
#         self.im = self.ax.imshow(self.images[0])
#         self.ax.axis('off')
#         self.update_plot()
  
#     def update_plot(self):
#         images=self.images_dict[self.file_prefix]['binary']
#         self.im.set_data(images[self.max_frame])
#         tracks_frame=self.tracks[self.tracks['frame']<=self.max_frame]
        
#         for id,group in tracks_frame.groupby("particle"):
#             self.ax.plot(group['x'],group['y'],lw=0.5,color='red')
#         self.fig.canvas.draw_idle() 


# In[21]:


# closeUp=CloseUpObject(images_dict)
# closeUp.container


# In[ ]:


# select_object=widgets.BoundedIntText(value=7,
#     min=0,
#     max=10,
#     step=2,
#     description='Object ID:',
#     disabled=False)


# In[ ]:


# from ipywidgets import interact, IntSlider

# traj=clean_tracks[clean_tracks["particle"]==46]
# #min_row,min_col,max_row,max_col=props.loc["bbox-0":"bbox-3"].values.astype(int)

# delta=10
# min_row,min_col=np.min(traj.loc[:,'bbox-0':'bbox-1'])
# max_row,max_col=np.max(traj.loc[:,'bbox-2':'bbox-3'])


# start_frame=np.min(traj.loc[:,'frame'])
# end_frame=np.max(traj.loc[:,'frame'])
# file_prefix=traj.file_name.values[0]

# images=images_dict[file_prefix]["img"]
# binary=images_dict[file_prefix]["binary"]
# min_row=np.max([min_row-delta,0])
# min_col=np.max([min_col-delta,0])
# max_row=np.min([max_row+delta,images[0].shape[0]])
# max_col=np.min([max_col+delta,images[0].shape[1]])
               
# traj_img=images[:,min_row:max_row,min_col:max_col]
# traj_seg=binary[:,min_row:max_row,min_col:max_col]



# def plot_plane(frame):
#     fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(20,5))
#     # particle image
#     ax[0].imshow(traj_img[frame],cmap='gray')
#     ax[0].axis("off")
    
#     # special points
#     list_peaks=ast.literal_eval(traj.peaks[frame])
#     for peak in list_peaks:
#         row,col,intensity=peak
#         row=row+(traj['bbox-0'][frame]-min_row)
#         col=col+(traj['bbox-1'][frame]-min_col)
#         ax[0].plot(col,row,'o',color="red",markersize=3)
    
#     # particle image
#     ax[1].imshow(traj_seg[frame],cmap='gray')
#     ax[1].axis("off")
        
#     # area vs frame plot
#     ax[2].plot(traj.frame,traj.orientation,'-o')
#     ax[2].axvline(x=frame,color='red')
#     ax[2].set_title("orientation")
#     ax[2].set_xlabel("frame")
#     ax[2].set_ylabel("area")
    
# interact(plot_plane,frame = IntSlider(min=start_frame,max=end_frame,step=1,value=0))

