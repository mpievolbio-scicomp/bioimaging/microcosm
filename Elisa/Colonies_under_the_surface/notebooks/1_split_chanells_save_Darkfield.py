#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import skimage
import skimage.io as io


# In[4]:


FileList=glob('../data/*.tif')
FileList


# In[7]:


for file_name in FileList:
    file_name_prefix=file_name.split('/')[-1].split(".tif")[0]
    
    img=io.imread(file_name)
    print(img.shape)
    chanel_bright=img[:,0,:,:] # Bright Field
    chanel_dark=img[:,1,:,:]   # Dark Field
    
    io.imsave("../data/"+file_name_prefix+"_Dark.tif",chanel_dark,check_contrast=False,plugin='tifffile')


# In[2]:


img=io.imread('../data/43125.tif')


# In[3]:


img.shape


# In[ ]:




