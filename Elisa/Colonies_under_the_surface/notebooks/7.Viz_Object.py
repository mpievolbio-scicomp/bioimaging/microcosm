#!/usr/bin/env python
# coding: utf-8

# In[8]:


import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns

import skimage.io as io
from skimage import util

from glob import glob
import trackpy

import ast
from ipywidgets import widgets


from ipywidgets.embed import embed_minimal_html


# ## Read data

# In[9]:


file_list=glob("../data/*_Dark_prepared.tif")
images_dict={}
for file_name in file_list:
    images_prepared=io.imread(file_name)
    file_prefix=os.path.split(file_name)[1].split('.')[0].split("_prepared")[0]
    
    binary_file=file_name.split(".tif")[0]+'_segmented'+'.tif'
    images_binary=io.imread(binary_file)
    tracks=pd.read_csv("../data/tracks_overlay_id_"+file_prefix+'.csv')
    tracks_big=pd.read_csv("../data/tracks_overlay_id_"+file_prefix+'_big.csv')
    img_dict={"img":images_prepared,"binary":images_binary,"tracks":tracks,"tracks_big":tracks_big}
    
    images_dict[file_prefix]=img_dict


# ## Object viz

# In[27]:


file_prefix="43122_Dark"
particle=4

# 'major_axis_length','minor_axis_length', 'orientation', 'area', 'perimeter', 'eccentricity',
#'max_intensity','mean_intensity', 'min_intensity', 'compactness', 'circularity','contrast',  'file_name', 'particle'
column_name='area'

###========================================####
tracks=images_dict[file_prefix]["tracks_big"]
images=images_dict[file_prefix]["img"]
binary=images_dict[file_prefix]["binary"]


traj=tracks[tracks["particle"]==particle]

delta=10
min_row,min_col=np.min(traj.loc[:,'bbox-0':'bbox-1'])
max_row,max_col=np.max(traj.loc[:,'bbox-2':'bbox-3'])

start_frame=np.min(traj.loc[:,'frame'])
end_frame=np.max(traj.loc[:,'frame'])

min_row=np.max([min_row-delta,0])
min_col=np.max([min_col-delta,0])
max_row=np.min([max_row+delta,images[0].shape[0]])
max_col=np.min([max_col+delta,images[0].shape[1]])
               
traj_img=images[:,min_row:max_row,min_col:max_col]
traj_seg=binary[:,min_row:max_row,min_col:max_col]


###========================================####
def plot_plane(frame):
    fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(20,5))
    # particle image
    ax[0].imshow(traj_img[frame],cmap='gray')
    ax[0].axis("off")
    
#     # special points
#     list_peaks=ast.literal_eval(traj.peaks[frame])
#     for peak in list_peaks:
#         row,col,intensity=peak
#         row=row+(traj['bbox-0'][frame]-min_row)
#         col=col+(traj['bbox-1'][frame]-min_col)
#         ax[0].plot(col,row,'o',color="red",markersize=3)
    
    # particle image
    ax[1].imshow(traj_seg[frame],cmap='gray')
    ax[1].axis("off")
        
    # area vs frame plot
    
    ax[2].plot(traj.frame,traj[column_name],'-o')
    ax[2].axvline(x=frame,color='red')
    ax[2].set_title(column_name)
    ax[2].set_xlabel("frame")
    ax[2].set_ylabel(column_name)
    plt.show()

intSlider=widgets.IntSlider(min=start_frame,max=end_frame,step=1,value=0)    
play_w=widgets.Play(value=0,min=start_frame,max=end_frame,interval=200,description="Press play",disabled=False)
widgets.jslink((play_w, 'value'), (intSlider, 'value'))


int_w=widgets.interactive(plot_plane,frame = intSlider)
container=widgets.HBox([int_w])

display(container)

#from ipywidgets.embed import embed_minimal_html
embed_minimal_html('../results/export.html', views=container, title='Widgets export')


# In[11]:


#from ipywidgets import IntSlider
from ipywidgets.embed import embed_minimal_html

#slider = IntSlider(value=40)
embed_minimal_html('../results/export.html', views=[container], title='Widgets export')


# In[12]:


get_ipython().run_line_magic('pinfo', 'widgets.interactive')


# In[23]:


get_ipython().system('jupyter nbconvert 7.Viz_Object.ipynb --to html_embed')


# ## Animation 

# In[22]:


# %matplotlib inline
# import matplotlib.pyplot as plt
# plt.rcParams["animation.html"] = "html5"
# import matplotlib.animation
# import numpy as np


# def plot_plane(frame):
#     fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(20,5))
#     # particle image
#     ax[0].imshow(traj_img[frame],cmap='gray')
#     ax[0].axis("off")
    
# #     # special points
# #     list_peaks=ast.literal_eval(traj.peaks[frame])
# #     for peak in list_peaks:
# #         row,col,intensity=peak
# #         row=row+(traj['bbox-0'][frame]-min_row)
# #         col=col+(traj['bbox-1'][frame]-min_col)
# #         ax[0].plot(col,row,'o',color="red",markersize=3)
    
#     # particle image
#     ax[1].imshow(traj_seg[frame],cmap='gray')
#     ax[1].axis("off")
        
#     # area vs frame plot
    
#     ax[2].plot(traj.frame,traj[column_name],'-o')
#     ax[2].axvline(x=frame,color='red')
#     ax[2].set_title(column_name)
#     ax[2].set_xlabel("frame")
#     ax[2].set_ylabel(column_name
                     
                     
                     
                     
# t = np.linspace(start_frame,end_frame)
# x = np.sin(t)

# fig, ax = plt.subplots()
# l, = ax.plot([0,2*np.pi],[-1,1])

# animate = lambda i: l.set_data(t[:i], x[:i])

# ani = matplotlib.animation.FuncAnimation(fig, animate, frames=len(t))
# ani


# In[ ]:




