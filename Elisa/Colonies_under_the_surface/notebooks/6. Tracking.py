#!/usr/bin/env python
# coding: utf-8

# In[7]:


import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns

import skimage.io as io
from skimage import util

from glob import glob
import trackpy

import ast


# In[8]:


def viz_trajectories(images_binary,clean_tracks,tracks_folder):
    """
    viz all trajectories on the images sequence
    """

    for frame in range(images_binary.shape[0]):
        fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(20,20))
        # show image
        ax.imshow(images_binary[frame],cmap='gray')
        ax.axis('off')
        
        # viz tracks
        tracks_frame=tracks[tracks['frame']<=frame]
        for id,group in tracks_frame.groupby("particle"):
            ax.plot(group['x'],group['y'],lw=2,color='red')
            
        
        fig.savefig(tracks_folder+"/im_traj_"+str(frame)+".png",bbox_inches='tight')


# ## Read data

# In[3]:


file_list=glob("../data/*_Dark_prepared.tif")
images_dict={}
for file_name in file_list:
    images_prepared=io.imread(file_name)
    binary_file=file_name.split(".tif")[0]+'_segmented'+'.tif'
    images_binary=io.imread(binary_file)
    img_dict={"img":images_prepared,"binary":images_binary}
    file_prefix=os.path.split(file_name)[1].split('.')[0].split("_prepared")[0]
    images_dict[file_prefix]=img_dict


# In[4]:


Data=pd.read_csv("../data/Measurements.csv")


# In[5]:


for fname, df in Data.groupby("file_name"):
    print(fname)


# ## Tracking all objects

# In[6]:


tracks = trackpy.link_df(df,search_range=35.5)
# filter out trajectories less than 3 frames
clean_tracks = trackpy.filter_stubs(tracks,threshold=3)
clean_tracks.to_csv("../data/"+"tracks_overlay_id_{}.csv".format("43125_Dark"))

tracks_folder='../data/'+file_prefix+"_tracks"
os.mkdir(tracks_folder)
viz_trajectories(images_binary,clean_tracks,tracks_folder)


# ## Tracking big objects

# In[51]:


file_prefix="43125_Dark"
df=Data[Data["file_name"]=="43125_Dark"]
images_binary=io.imread("../data/"+file_prefix+"_prepared_segmented.tif")
df=df.rename(columns={'centroid-1':"x",'centroid-0':"y"})

# tracking only big objects
df_big=df[df['area']>500]
print(df_big.shape)

tracks = trackpy.link_df(df_big,search_range=35.5)
# filter out trajectories less than 3 frames
clean_tracks = trackpy.filter_stubs(tracks,threshold=3)
clean_tracks.to_csv("../data/"+"tracks_overlay_id_{}_big.csv".format("43125_Dark"))

tracks_folder='../data/'+file_prefix+"_tracks_big"
os.mkdir(tracks_folder)
viz_trajectories(images_binary,clean_tracks,tracks_folder)


# In[505]:


from ipywidgets import interact, IntSlider

traj=clean_tracks[clean_tracks["particle"]==46]
#min_row,min_col,max_row,max_col=props.loc["bbox-0":"bbox-3"].values.astype(int)

delta=10
min_row,min_col=np.min(traj.loc[:,'bbox-0':'bbox-1'])
max_row,max_col=np.max(traj.loc[:,'bbox-2':'bbox-3'])


start_frame=np.min(traj.loc[:,'frame'])
end_frame=np.max(traj.loc[:,'frame'])
file_prefix=traj.file_name.values[0]

images=images_dict[file_prefix]["img"]
binary=images_dict[file_prefix]["binary"]
min_row=np.max([min_row-delta,0])
min_col=np.max([min_col-delta,0])
max_row=np.min([max_row+delta,images[0].shape[0]])
max_col=np.min([max_col+delta,images[0].shape[1]])
               
traj_img=images[:,min_row:max_row,min_col:max_col]
traj_seg=binary[:,min_row:max_row,min_col:max_col]



def plot_plane(frame):
    fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(20,5))
    # particle image
    ax[0].imshow(traj_img[frame],cmap='gray')
    ax[0].axis("off")
    
    # special points
    list_peaks=ast.literal_eval(traj.peaks[frame])
    for peak in list_peaks:
        row,col,intensity=peak
        row=row+(traj['bbox-0'][frame]-min_row)
        col=col+(traj['bbox-1'][frame]-min_col)
        ax[0].plot(col,row,'o',color="red",markersize=3)
    
    # particle image
    ax[1].imshow(traj_seg[frame],cmap='gray')
    ax[1].axis("off")
        
    # area vs frame plot
    ax[2].plot(traj.frame,traj.orientation,'-o')
    ax[2].axvline(x=frame,color='red')
    ax[2].set_title("orientation")
    ax[2].set_xlabel("frame")
    ax[2].set_ylabel("area")
    
interact(plot_plane,frame = IntSlider(min=start_frame,max=end_frame,step=1,value=0))


# In[501]:


traj.orientation


# In[485]:


d_orientation=np.diff(traj.orientation)
d_orientation=np.where((d_orientation)>2,d_orientation-np.pi,d_orientation)
d_orientation=np.where((d_orientation)<-2,d_orientation+np.pi,d_orientation)
plt.plot(np.cumsum(d_orientation))


# In[500]:


np.cumsum(d_orientation)


# In[497]:


angle=np.insert(np.cumsum(d_orientation),traj.orientation.values[0],1)


# ## Area

# In[548]:


def area_analysis(traj,ax):
    """
    linear regression
    input: trajectories 
    output: dictionaries
            "particle"- particle id
            "traj_len"-len of the trajectories
            "slope", - slope of the linear regression
            "intersept",
            "MSE":mean_squared_error,
            "R2":r2_score
    """
    
    particle=traj['particle'].values[0]
    time=traj['frame']
    traj_len=time.shape[0]
    area=traj['area']
    
    # linear regression
    result,area_predict=linear_regression(time,area,'area')
    result["particle"]=particle
    result["traj_len"]=traj_len
    
    # max area difference
    area_diff_result=max_diff(traj)
    result.update(area_diff_result)

    ax.plot(time, area, color='blue')
    ax.plot(time, area_predict, color='red', linewidth=3,label="p={}\nR2={:.2f}".format(particle,result["R2_area"]))
    ax.legend()
    
    return result


def linear_regression(time,prop,prefix):
    """
    Linear regression time vs properties
    (could be area,orientation,compactness)
    
    """
    from sklearn import linear_model
    from sklearn.metrics import mean_squared_error, r2_score
    
    # reshape required for LinearRegression
    time=time.values.reshape(-1, 1)
    
    # Create linear regression object
    regr = linear_model.LinearRegression(fit_intercept=True)
    regr.fit(time, prop)
    
    # Make predictions using the testing set
    prop_predict = regr.predict(time)
    
    
    #     # The coefficients
    #     print('Coefficients: \n', regr.coef_)
    #     # The mean squared error
    #     print('Mean squared error: %.2f'% mean_squared_error(area, area_predict))
    #     # The coefficient of determination: 1 is perfect prediction
    #     print('Coefficient of determination: %.2f'% r2_score(area,area_predict))

    # Plot outputs
    result={"slope_{}".format(prefix):regr.coef_[0], 
                   "intersept_{}".format(prefix):regr.intercept_,
                   "MSE_{}".format(prefix):mean_squared_error(prop,prop_predict),
                   "R2_{}".format(prefix):r2_score(prop,prop_predict)}
    
    return result,prop_predict
   



def max_diff(traj):
    """
    calculate maximum difference area[t+1]-area[t]
              relative maximum difference area[t+1]-area[t]/area[t]
    input:  trajectory
    output: max_diff,
            time_max_diff
    """
    d_area=np.diff(traj.area)
    max_diff=np.max(d_area)
    time_max_diff=np.argmax(d_area)
    
    relative_diff=d_area/traj.area.iloc[0:-1]
    max_relative_diff=np.max(relative_diff)
    time_relative_max_diff=np.argmax(relative_diff)
    
    result={'max_relative_diff_area':max_relative_diff,
            'time_relative_max_diff_are':time_relative_max_diff}
    return result

Ngroups=clean_tracks.groupby('particle').ngroups
fig,ax=plt.subplots(nrows=8,ncols=10,figsize=(20,20))
axx=ax.ravel()

counter=0
Area=list()
for id, traj in clean_tracks.groupby('particle'):
    dict_entry=area_analysis(traj,axx[counter])
    Area.append(dict_entry)
    counter=counter+1
    
Area=pd.DataFrame(Area)


# In[454]:


sns.scatterplot(data=Linear_regression_slope,x="particle",y='max_diff_area')


# In[456]:


sns.scatterplot(data=Linear_regression_slope,x="particle",y='max_relative_diff_area')


# In[ ]:


Area[Area['max_relative_diff']>0.1]


# In[457]:


traj=clean_tracks[clean_tracks['particle']==17]
d_area=np.diff(traj.area)
d=pd.DataFrame(d_area,columns=['d_area'])
fig,ax=plt.subplots(nrows=1,ncols=1)
ax.plot(d)
ax.plot(d.rolling(3).mean(),color='red')


# ## Center mass

# In[424]:


def center_mass(traj,ax):
    """
    anylise center of mass movement
    
    max_distance_origin - maximum distance from the start of the trajectory
    x_mean - descriptor to check location within full frame
    y_mean - descriptor to check location within full frame
    mean_speed - mean speed
    
    """
    particle=traj['particle'].values[0]
    time=traj['frame'].values
    start_x=traj['x'].values[0]
    start_y=traj['y'].values[0]
    
    r=np.sqrt((traj['x'].values-start_x)**2+(traj['y'].values-start_y)**2)
    
    # max distance from origin
    max_distance_origin=np.max(r)
    
    # location relative to the full frame
    x_mean=np.mean(traj['x'].values)
    y_mean=np.mean(traj['x'].values)
    
    # speed 
    speed=np.diff(r)
    mean_speed=np.mean(speed)
    
    ax.plot(traj.frame,r,label=str(particle))
    ax.legend()
    
    result={'particle':particle,
            'max_distance_origin':max_distance_origin,
            'x_mean':x_mean,
            'y_mean':y_mean,
            'mean_speed':mean_speed
           }
    return result


    

Center_Mass=list()
Ngroups=clean_tracks.groupby('particle').ngroups
fig,ax=plt.subplots(nrows=8,ncols=10,figsize=(20,20))
axx=ax.ravel()

counter=0
for id, traj in clean_tracks.groupby('particle'):
    result=center_mass(traj,axx[counter])
    Center_Mass.append(result)
    counter=counter+1
    
Center_Mass=pd.DataFrame(Center_Mass)


# In[423]:


Center_Mass[Center_Mass['max_distance_origin']>30]


# In[420]:


sns.scatterplot(data=Center_Mass,x='particle',y='max_distance_origin')


# ## Orientation

# In[506]:


def orientation(traj,ax):
    particle=traj['particle'].values[0]
    time=traj['frame'].values
    
    # elivate big jumps of angle 
    # due to the transition from [pi/2,delta+pi/2] or [-pi/2,-delta-pi/2]
    delta=2
    d_orientation=np.diff(traj.orientation)
    d_orientation=np.where((d_orientation)>delta,d_orientation-np.pi,d_orientation)
    d_orientation=np.where((d_orientation)<-delta,d_orientation+np.pi,d_orientation)
    angle=np.cumsum(d_orientation)
    angle=angle*180/np.pi
    
    #========================#
    # median eccentricity
    eccentricity=np.median(traj['eccentricity'])
    
    # max diff angle
    max_diff_angle=np.max(np.abs(d_orientation))
    
    # cumsum
    cumsum_angle=angle[-1]
    #========================#
    
    result={"particle":particle,
            "med_eccentricity":eccentricity,
            "max_diff_angle":max_diff_angle,
            "cumsum_angle":cumsum_angle}
    
    
    
    #angle=angle.rolling(3).median().values
    s = pd.Series(angle)
    #pd.plotting.autocorrelation_plot(s,ax=ax)
    #ax.legend(str(particle))
    ax.plot(traj.frame[1:],angle,label='particle={:}\ne={:.2f}'.format(particle,np.median(eccentricity)))
    ax.legend()
    
    
    return result

Ngroups=clean_tracks.groupby('particle').ngroups
fig,ax=plt.subplots(nrows=8,ncols=10,figsize=(20,20))
axx=ax.ravel()

counter=0
Orientation=list()
for id, traj in clean_tracks.groupby('particle'):
    result=orientation(traj,axx[counter])
    Orientation.append(result)
    counter=counter+1
    
Orientation=pd.DataFrame(Orientation)


# In[509]:


Orientation.hist()


# ## Compactness
# $$ compactness=\frac {perimeter^2}{area} $$

# In[519]:


def compactness(traj,ax,prefix):
    particle=traj['particle'].values[0]
    time=traj['frame']
    
    result,predict=linear_regression(time,traj['compactness'],'compactness')
    result.update({"particle":particle})
    
    ax.plot(traj.frame,traj['compactness'])
    ax.plot(traj.frame,predict,label='particle={:}'.format(particle))
    ax.legend()
    return result

Ngroups=clean_tracks.groupby('particle').ngroups
fig,ax=plt.subplots(nrows=8,ncols=10,figsize=(20,20))
axx=ax.ravel()

counter=0
Compactness=list()
for id, traj in clean_tracks.groupby('particle'):
    result=compactness(traj,axx[counter],'compactness')
    Compactness.append(result)
    counter=counter+1

Compactness=pd.DataFrame(Compactness)


# In[526]:


Center_Mass


# In[460]:


Compactness.hist(bins=100)


# ## Assemble measurements

# In[549]:


from functools import reduce
df_list=[Area,Center_Mass,Orientation,Compactness]
df_merged = reduce(lambda  left,right: pd.merge(left,right,on=['particle'],
                                            how='outer'), df_list)


# In[534]:


Compactness[Compactness['particle']==10]


# In[537]:


df_merged [df_merged ['particle']==10]


# In[553]:


df_merged.columns


# In[556]:


cols=['slope_area', 'intersept_area', 'MSE_area', 'R2_area', 'particle',
       'traj_len', 'max_relative_diff_area', 'time_relative_max_diff_are',
       'max_distance_origin', 'x_mean', 'y_mean', 'mean_speed',
       'med_eccentricity', 'max_diff_angle', 'cumsum_angle',
       'slope_compactness', 'intersept_compactness', 'MSE_compactness',
       'R2_compactness']


# In[558]:


[cols]


# In[557]:


#corrmat = df_merged[list_columns].corr()
#f, ax = plt.subplots(figsize=(12, 9))
k=10
cols = corrmat.nlargest(k, 'slope_area')['slope_area'].index
cm = np.corrcoef(df_merged[cols].values.T)
sns.set(font_scale=1.25)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
plt.show()


# In[543]:


sns.set(font_scale=1.5)
corrmat["slope_area"].plot(kind="bar",figsize=(30,10))


# In[ ]:




