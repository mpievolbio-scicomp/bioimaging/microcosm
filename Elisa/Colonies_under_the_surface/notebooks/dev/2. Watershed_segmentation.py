#!/usr/bin/env python
# coding: utf-8

# In[8]:


import numpy as np
from matplotlib import pyplot as plt 

import skimage.io as io
from skimage import util
import skimage
from skimage.morphology import disk,white_tophat


from skimage.filters import laplace,sobel,gaussian
from skimage.filters.rank import gradient

from skimage.exposure import rescale_intensity,equalize_adapthist
from skimage import util
import skimage.morphology as morph

from scipy.signal import find_peaks
from matplotlib.patches import Rectangle
import seaborn as sns

import pandas as pd

plt.style.use('default')


# In[ ]:


def img_compare(img1,img2,label1,label2):
    fig, ax = plt.subplots(1,2,figsize=(20, 20))
    ax[0].imshow(img1)
    ax[0].set_title(label1)
    ax[1].imshow(img2, cmap=plt.cm.gray)
    ax[1].set_title(label2)
    ax[1].axis('off')


# In[10]:


images.shape


# # Primary object detection

# ## Image preparation

# In[15]:


images=io.imread("../data/43125_Dark.tif")

# decrease noise
denoised=np.zeros_like(images)
background=np.zeros_like(images)
images_prepared=np.zeros_like(images,dtype="float32")

for frame in range(images.shape[0]):
    print(frame)
    denoised[frame]=skimage.filters.gaussian(images[frame],1.5)
    background[frame]=skimage.filters.gaussian(images[frame],75)
    images_prepared[frame]=denoised[frame]-background[frame]
    


# In[16]:


io.imsave("../data/43125_Dark_denoised.tif",denoised)
io.imsave("../data/43125_Dark_bg.tif",background)
io.imsave("../data/43125_Dark_images_prepared.tif",images_prepared)


# ## Outsu threshold

# In[22]:


from skimage.filters import threshold_otsu
Threshold=threshold_otsu(images_prepared)
images_binary=np.zeros_like(images)
images_binary[images_prepared>Threshold]=255


# In[20]:


io.imsave("../data/43125_Dark_images_binary.tif",images_binary)


# In[23]:


plt.imshow(images_prepared[10])


# # Segmentation secondary object

# In[141]:


watershed_img=1-denoised
watershed_img=watershed_img-np.min(watershed_img)


# In[161]:


# markers
from skimage.feature import peak_local_max
from scipy import ndimage as ndi

coordinates=peak_local_max(denoised, min_distance=10,threshold_rel=0.25)
maxima_image=np.zeros_like(img_test)
maxima_image[coordinates[:, 0], coordinates[:, 1]]=1

labeled_maxima, object_count = ndi.label(maxima_image, np.ones((3, 3), bool))
markers = np.zeros(watershed_img.shape, np.int32)
markers[labeled_maxima > 0] = -labeled_maxima[labeled_maxima > 0]

watershed_boundaries = segmentation.watershed(
                connectivity=np.ones((3, 3), bool),
                image=watershed_img,
                markers=markers,
                mask=labeled_image != 0)



fig,ax= plt.subplots(nrows=1,ncols=2,figsize=(20,20))
ax[0].imshow(img_test)
ax[0].plot(coordinates[:, 1], coordinates[:, 0], 'r.')
ax[1].imshow(watershed_boundaries,cmap=plt.cm.jet)


# In[162]:


fig,ax= plt.subplots(nrows=1,ncols=2,figsize=(20,20))
ax[0].imshow(img_test[500:700,500:800])
#ax[0].plot(coordinates[:, 1], coordinates[:, 0], 'r.')
ax[1].imshow(watershed_boundaries[500:700,500:800])

