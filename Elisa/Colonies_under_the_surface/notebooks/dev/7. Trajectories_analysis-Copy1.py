#!/usr/bin/env python
# coding: utf-8

# In[16]:


import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns

import skimage.io as io
from skimage import util

from glob import glob
import trackpy

import ast
from ipywidgets import interact, widgets


# import code from the src
### import code from src folder
import sys
sys.path.append('../src/')
import chow_test_v

import warnings
warnings.filterwarnings('ignore')

from sklearn.metrics import r2_score, median_absolute_error, mean_absolute_error
from sklearn.metrics import median_absolute_error, mean_squared_error, mean_squared_log_error


# ## Read data

# In[17]:


file_list=glob("../data/*_Dark_prepared.tif")
images_dict={}
for file_name in file_list:
    images_prepared=io.imread(file_name)
    file_prefix=os.path.split(file_name)[1].split('.')[0].split("_prepared")[0]
    
    binary_file=file_name.split(".tif")[0]+'_segmented'+'.tif'
    images_binary=io.imread(binary_file)
    tracks=pd.read_csv("../data/tracks_overlay_id_"+file_prefix+'.csv')
    tracks_big=pd.read_csv("../data/tracks_overlay_id_"+file_prefix+'_big.csv')
    img_dict={"img":images_prepared,"binary":images_binary,"tracks":tracks,"tracks_big":tracks_big}
    
    images_dict[file_prefix]=img_dict


# ## Select Experiment

# In[223]:


# ## Plot all experiments
# for file_prefix in images_dict.keys():
#     tracks=images_dict[file_prefix]["tracks_big"]
# #     images=images_dict[file_prefix]["img"]
# #     binary=images_dict[file_prefix]["binary"]

#     Ngroups=tracks.groupby('particle').ngroups
#     Nrows=np.int(np.ceil(Ngroups/10))
#     fig,ax=plt.subplots(nrows=Nrows,ncols=10,figsize=(40,Nrows*5))
#     plt.suptitle(file_prefix)
#     axx=ax.ravel()

#     counter=0
#     #Area=list()
#     for id, traj in tracks.groupby('particle'):
#         #dict_entry=area_analysis(traj,axx[counter])
#         d_area=np.diff(traj.area)
#         d_area=pd.DataFrame(d_area).rolling(5).std()
#         axx[counter].plot(d_area,label=traj["particle"].iloc[0])
#         axx[counter].legend()
#         Area.append(dict_entry)
#         counter=counter+1
    
#     #fig.savefig("../results/Area_"+file_prefix+".png",bbox_inches='tight')
# #Area=pd.DataFrame(Area)


# ## Area
# 
# * The objects have dynamic behavior. The area measurements are fluctuate with a time. My try to divide objects on subunits does not work  (geometry method, local-maximum method) . 
# * Area are generally grow in two ways: linear growth of the object itself (linearly?) and aggregation.
# 

# In[241]:


def area_analysis(traj,ax):
    """
    linear regression
    input: trajectories 
    output: dictionaries
            "particle"- particle id
            "traj_len"-len of the trajectories
            "slope", - slope of the linear regression
            "intersept",
            "MSE":mean_squared_error,
            "R2":r2_score
    """
    traj=traj.sort_values('frame')
    particle=traj['particle'].values[0]
    time=traj['frame']
    traj_len=time.shape[0]
    area=traj['area']
    
    # detect anomalies
    anomalies=simple_anomaly_detector(traj,ax,5,1.96,plot_flag=True)

    # linear regression
    if len(anomalies)==0:
        result,area_predict=linear_regression(time,area,'area')
        result["interval_counter"]=1
        ax.plot(time, area_predict, color='red', linewidth=3,label="p={}\nR2={:.2f}".format(particle,result["R2"]))
        ax.legend()
    else:
        result=fit_area_with_anomalies(traj,anomalies,ax)
        
    
    return pd.DataFrame(result)
        
        



def simple_anomaly_detector(traj,ax,window=5,scale=1.96,plot_flag=False):
    """
    simple anomaly detector
    """
    break_id=[]
    
    # set minimal size of trajectories for anomaly detection
    if (traj.shape[0]>15):
        tt=traj.copy()
        tt=tt.sort_values('frame')
        tt=tt.set_index('frame')
        tt=tt['area']
    
        
        # find candidates for anomaly
        rolling_mean = tt.rolling(window=5).mean()
        mae=mean_absolute_error(tt[window:], rolling_mean[window:])
        deviation = np.std(tt[window:] - rolling_mean[window:])
        lower_bond = rolling_mean - (mae + scale * deviation)
        upper_bond = rolling_mean + (mae + scale * deviation)
        
        anomalies=pd.Series('NA',index=tt.index)
        anomalies_lower = tt[tt<lower_bond]
        anomalies_upper = tt[tt>upper_bond]
       
        if (plot_flag==True):
            # plot anomalies
            #fig,ax=plt.subplots(figsize=(8,5))
            ax.set_title("Moving average\n window size = {}".format(window))
            ax.plot(rolling_mean, "g") # label="Rolling mean trend"
            ax.plot(tt, "black", label=traj['particle'].values[0])
            ax.plot(upper_bond, "r--") #label="Upper Bond / Lower Bond"
            ax.plot(lower_bond, "r--")
            ax.plot(anomalies_upper, "go", markersize=10)
            ax.plot(anomalies_lower, "go", markersize=10)
            ax.legend()
            
        ## chow test

        
        for id in np.hstack([anomalies_upper.index,anomalies_lower]):
            #print(id)
            
            part1=tt[0:id]
            x1=part1.index.values
            y1=part1.values
            #print(part1,x1,y1)
    
            part2=tt[(id+1):]
            x2=part2.index.values
            y2=part2.values
            #print(part2,x2,y2)
            
            if (len(y1)>3)&(len(y2)>3):
                is_break,p_val= chow_test_v.p_value(y1, x1, y2, x2)
                if is_break==1:
                    #print("hear")
                    break_id.append(id)
                    
                    if (plot_flag==True):
                        ax.plot(id,tt.loc[id],"ro", markersize=10)
            


            
    return break_id


def linear_regression(time,prop,prefix):
    """
    Linear regression time vs properties
    (could be area,orientation,compactness)
    
    """
    from sklearn import linear_model
    from sklearn.metrics import mean_squared_error, r2_score
    
    # reshape required for LinearRegression
    time=time.values.reshape(-1, 1)
    
    # Create linear regression object
    regr = linear_model.LinearRegression(fit_intercept=True)
    regr.fit(time, prop)
    
    # Make predictions using the testing set
    prop_predict = regr.predict(time)
    
    
    #     # The coefficients
    #     print('Coefficients: \n', regr.coef_)
    #     # The mean squared error
    #     print('Mean squared error: %.2f'% mean_squared_error(area, area_predict))
    #     # The coefficient of determination: 1 is perfect prediction
    #     print('Coefficient of determination: %.2f'% r2_score(area,area_predict))

    # Plot outputs
    result={"particle":prefix,"slope":regr.coef_[0], 
                   "intersept":regr.intercept_,
                   "MSE":mean_squared_error(prop,prop_predict),
                   "R2":r2_score(prop,prop_predict)}
    
    return result,prop_predict
   



def fit_area_with_anomalies(traj,anomalies,ax):
    
    ##====================================##
    ##        Select intervals for fit    ##
    ##====================================##
    #the length of the interval should be >5 frames
    
    def check_interval_length(break_intervals,inter):
        if inter.size>5:
            break_intervals.append(inter)
            
    break_intervals=[]
    # start interval
    interval_start=np.arange(0,anomalies[0])
    check_interval_length(break_intervals,interval_start)
    # middel intervals
    for i in range(0,len(anomalies)-1):
        interval=np.arange(anomalies[i]+1,anomalies[i+1])
        check_interval_length(break_intervals,interval)
    # end interval
    interval_end=np.arange(anomalies[-1],traj["particle"].shape[0]+1)
    check_interval_length(break_intervals,interval_end)
    

    
    ##=============================================##
    ##        Fit intervals by linear regression   ##
    ##=============================================##
    Result=[]
    interval_counter=1
    for inter in break_intervals:
        start=inter[0]
        end=inter[-1]
        peace_to_fit=traj[(traj["frame"]>=start)&(traj["frame"]<=end)]
        result,prop_predict=linear_regression(peace_to_fit["frame"],peace_to_fit["area"],str(traj["particle"].values[0]))
        result["interval_counter"]=interval_counter
        Result.append(result)
        interval_counter=interval_counter+1
        ax.plot(peace_to_fit["frame"],prop_predict,color='red',lw=4)
        
    return Result


# In[245]:


file_prefix="43125_Dark"
tracks=images_dict[file_prefix]["tracks_big"]
images=images_dict[file_prefix]["img"]
binary=images_dict[file_prefix]["binary"]


Ngroups=tracks.groupby('particle').ngroups
Nrows=np.int(np.ceil(Ngroups/5))

sns.set(font_scale=2)
fig,ax=plt.subplots(nrows=Nrows,ncols=5,figsize=(40,Nrows*5))
axx=ax.ravel()

counter=0
Area=pd.DataFrame()
for id, traj in tracks.groupby('particle'):
    df_result=area_analysis(traj,axx[counter])
    Area=pd.concat([Area,df_result])
    #anomalies=simple_anomaly_detector(traj,axx[counter],5,1.96,plot_flag=True)
    counter=counter+1


# In[244]:


Area


# In[218]:


traj=tracks[tracks['particle']==4]
window=5
scale=1.96


fig,ax=plt.subplots(figsize=(15,5))
anomalies=simple_anomaly_detector(traj,ax,5,1.96,plot_flag=True)
print(anomalies)
result=fit_area_with_anomalies(traj,anomalies,ax)
#simple_anomaly_detector(traj,ax,5,1.96,plot_flag=True)




# break_intervals=intervals_for_fit(anomalies,traj["particle"].shape[0])
# for inter in break_intervals:
#     start=inter[0]
#     end=inter[-1]
#     peace_to_fit=traj[(traj["frame"]>=start)&(traj["frame"]<=end)]
#     result,prop_predict=linear_regression(peace_to_fit["frame"],peace_to_fit["area"],str(traj["particle"].values[0]))
#     print(result)
#     ax.plot(peace_to_fit["frame"],prop_predict,color='blue')


# In[ ]:


# select=['slope_area', 'intersept_area','R2_area','traj_len', 'max_relative_diff_area', 'time_relative_max_diff_are']
# sns.pairplot(data=Area[select])


# ## Center mass

# In[ ]:


def center_mass(traj,ax):
    """
    anylise center of mass movement
    
    max_distance_origin - maximum distance from the start of the trajectory
    x_mean - descriptor to check location within full frame
    y_mean - descriptor to check location within full frame
    mean_speed - mean speed
    
    """
    particle=traj['particle'].values[0]
    time=traj['frame'].values
    start_x=traj['x'].values[0]
    start_y=traj['y'].values[0]
    
    r=np.sqrt((traj['x'].values-start_x)**2+(traj['y'].values-start_y)**2)
    
    # max distance from origin
    max_distance_origin=np.max(r)
    
    # location relative to the full frame
    x_mean=np.mean(traj['x'].values)
    y_mean=np.mean(traj['x'].values)
    
    # speed 
    speed=np.diff(r)
    mean_speed=np.mean(speed)
    
    ax.plot(traj.frame,r,label=str(particle))
    ax.legend()
    
    result={'particle':particle,
            'max_distance_origin':max_distance_origin,
            'x_mean':x_mean,
            'y_mean':y_mean,
            'mean_speed':mean_speed
           }
    return result


    

Center_Mass=list()
Ngroups=tracks.groupby('particle').ngroups
fig,ax=plt.subplots(nrows=8,ncols=10,figsize=(20,20))
axx=ax.ravel()

counter=0
for id, traj in tracks.groupby('particle'):
    result=center_mass(traj,axx[counter])
    Center_Mass.append(result)
    counter=counter+1
    
Center_Mass=pd.DataFrame(Center_Mass)


# In[ ]:


#Center_Mass[Center_Mass['max_distance_origin']>30]


# In[ ]:


Center_Mass.columns


# In[ ]:


Center_Mass[['max_distance_origin','mean_speed']].hist(layout=(1,2),figsize=(10,5),bins=50);


# ## Orientation

# In[ ]:


def orientation(traj,ax):
    particle=traj['particle'].values[0]
    time=traj['frame'].values
    
    # elivate big jumps of angle 
    # due to the transition from [pi/2,delta+pi/2] or [-pi/2,-delta-pi/2]
    delta=2
    d_orientation=np.diff(traj.orientation)
    d_orientation=np.where((d_orientation)>delta,d_orientation-np.pi,d_orientation)
    d_orientation=np.where((d_orientation)<-delta,d_orientation+np.pi,d_orientation)
    angle=np.cumsum(d_orientation)
    angle=angle*180/np.pi
    
    #========================#
    # median eccentricity
    eccentricity=np.median(traj['eccentricity'])
    
    # max diff angle
    max_diff_angle=np.max(np.abs(d_orientation))*180/np.pi
    
    # cumsum
    cumsum_angle=angle[-1]
    #========================#
    
    result={"particle":particle,
            "med_eccentricity":eccentricity,
            "max_diff_angle":max_diff_angle,
            "cumsum_angle":cumsum_angle}
    
    
    
    #angle=angle.rolling(3).median().values
    s = pd.Series(angle)
    #pd.plotting.autocorrelation_plot(s,ax=ax)
    #ax.legend(str(particle))
    ax.plot(traj.frame[1:],angle,label='particle={:}\ne={:.2f}'.format(particle,np.median(eccentricity)))
    ax.legend()
    
    
    return result

Ngroups=tracks.groupby('particle').ngroups
fig,ax=plt.subplots(nrows=8,ncols=10,figsize=(40,40))
axx=ax.ravel()

counter=0
Orientation=list()
for id, traj in tracks.groupby('particle'):
    result=orientation(traj,axx[counter])
    Orientation.append(result)
    counter=counter+1
    
Orientation=pd.DataFrame(Orientation)


# In[ ]:


Orientation[["med_eccentricity",'max_diff_angle','cumsum_angle']].hist(layout=(1,3),figsize=(15,5));


# ## Compactness
# $$ compactness=\frac {perimeter^2}{area} $$
# 
# Generally objects with a time become more regularly shaped

# In[ ]:


def compactness(traj,ax,prefix):
    particle=traj['particle'].values[0]
    time=traj['frame']
    
    result,predict=linear_regression(time,traj['compactness'],'compactness')
    result.update({"particle":particle})
    
    ax.plot(traj.frame,traj['compactness'])
    ax.plot(traj.frame,predict,label='particle={:}'.format(particle))
    ax.legend()
    return result

Ngroups=tracks.groupby('particle').ngroups
fig,ax=plt.subplots(nrows=8,ncols=10,figsize=(20,20))
axx=ax.ravel()

counter=0
Compactness=list()
for id, traj in tracks.groupby('particle'):
    result=compactness(traj,axx[counter],'compactness')
    Compactness.append(result)
    counter=counter+1

Compactness=pd.DataFrame(Compactness)


# In[ ]:


Compactness.columns


# In[ ]:


Compactness[['slope_compactness', 'intersept_compactness', 'R2_compactness']].hist(layout=(1,3),figsize=(15,5))


# ## Assemble measurements

# In[ ]:


from functools import reduce
df_list=[Area,Center_Mass,Orientation,Compactness]
df_merged = reduce(lambda  left,right: pd.merge(left,right,on=['particle'],
                                            how='outer'), df_list)


# In[ ]:


# list_columns=
# corrmat = df_merged[list_columns].corr()
# f, ax = plt.subplots(figsize=(12, 9))
# k=10
# cols = corrmat.nlargest(k, 'slope_area')['slope_area'].index
# cm = np.corrcoef(df_merged[cols].values.T)
# sns.set(font_scale=1.25)
# hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
# plt.show()


# In[ ]:


df_merged.columns


# ##  PCA

# In[ ]:


feature_names=['slope_area', 'intersept_area', 'MSE_area', 'R2_area',
               'traj_len', 'max_relative_diff_area',
       'max_distance_origin','mean_speed',
       'med_eccentricity', 'max_diff_angle', 'cumsum_angle',
       'slope_compactness', 'intersept_compactness', 'MSE_compactness',
       'R2_compactness']
df_merged[feature_names].hist(layout=(3,5),figsize=(20,15));


# In[ ]:


from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.mixture import GaussianMixture



X=df_merged[feature_names]

# scaling
scaler=MinMaxScaler()
Xscaled=scaler.fit_transform(X)

# PCA
ncomponents=3
pca=PCA(n_components=ncomponents)
X3d=pca.fit(Xscaled).transform(Xscaled)
print(pca.explained_variance_ratio_)
print(np.cumsum(pca.explained_variance_ratio_))


# Viz
sns.set(font_scale=1.7)
fig,ax=plt.subplots(nrows=1,ncols=3,figsize=(28,7))
variance_component=pd.DataFrame({"component":range(ncomponents),"Explained variance ratio":pca.explained_variance_ratio_})
sns.barplot(data=variance_component,x="component",y="Explained variance ratio",ax=ax[0])
ax[0].set_title("Explained variance ratio")
    
ax[1].plot(range(1,4),np.cumsum(pca.explained_variance_ratio_),lw=3,marker='o',markersize=12)
ax[1].set_title("PCA cumsum ")
ax[2].set_xlabel("component")
ax[2].set_ylabel("PCA cumsum")

ax[2].scatter(X3d[:,0].astype(float), X3d[:,1].astype(float),s=10,color='red')
ax[2].set_title("PCA")
ax[2].set_xlabel("PCA 1")
ax[2].set_ylabel("PCA 2")
    


# In[ ]:


import ipyvolume as ipv
ipv.figure()
#ipv.volshow(X3d.astype(float))
# add scatter plot
x=X3d[:,0]
y=X3d[:,1]
z=X3d[:,2]

ipv.scatter(x = X3d[:,0], y = X3d[:,1], z = X3d[:,2], size=1.5, color = "red")
# ipv.scatter(x = X3d[(cluster==1),0].astype(float), y = X3d[(cluster==1),1].astype(float), z = X3d[(cluster==1),2].astype(float), size=1.5, color = "blue")
ipv.show()


# In[ ]:




