#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import os

import skimage.io as io
from skimage import util

from glob import glob
from skimage import measure


# In[2]:


def img_compare(img_dict):
    n_figures=len(img_dict)
    
    fig, ax = plt.subplots(1,n_figures,figsize=(5*n_figures, 5))
    counter=0
    for label in img_dict:
        img=img_dict[label]
        ax[counter].imshow(img[0:500,0:500])
        ax[counter].set_title(label)
        ax[counter].axis('off')
        counter=counter+1
        


# # Outsu Thresholding

# In[3]:


def outsu_threshold(file_name):
    from skimage.filters import threshold_otsu
    """
    create list of images that used for multiprocessing
    """ 
    
    folder_to_save=os.path.split(file_name)[0]
    file_prefix=os.path.split(file_name)[1].split('.')[0]
    images_prepared=io.imread(file_name)
    
    images_binary=np.zeros_like(images_prepared,dtype=np.ubyte)
    threshold=threshold_otsu(images_prepared)
    images_binary[images_prepared>threshold]=255
    
    io.imsave(os.path.join(folder_to_save,file_prefix+"_segmented.tif"),images_binary)
    img_dict={"img":images_prepared,"binary":images_binary}
    
    return img_dict,file_prefix,threshold


# In[4]:


file_list=glob("../data/*_Dark_prepared.tif")
file_list


# In[5]:


thresholds=[]
images_dict={}
for file_name in file_list:
    img_dict,file_prefix,threshold=outsu_threshold(file_name)
    images_dict[file_prefix]=img_dict
    thr_dict={"file":file_name,"threshold":threshold}
    thresholds.append(thr_dict)
    
Thresholds=pd.DataFrame(thresholds)
Thresholds.to_csv("../data/Thresholds.csv")


# # Measure

# In[19]:


def props_measurement(images_binary,images):
    
    total_result_df=pd.DataFrame()
    for frame in range(images.shape[0]):
        label_image = measure.label(images_binary[frame])
        ## measurements
        props = measure.regionprops_table(label_image,images[frame],properties=
                                      ['label', 
                                       'centroid',
                                       'orientation',
                                       'area',
                                       'perimeter',
                                       'eccentricity',
                                       'bbox',])
        result_df=pd.DataFrame(props)
        
        # additional non-standart measurements
        result_df["frame"]=frame
        result_df["compactness"]=result_df['perimeter']**2/result_df['area']
        result_df["circulaty"]=4*np.pi*result_df['area']/result_df['perimeter']**2
        result_df["contrast"]=(result_df['max_intensity']-result_df['min_intensity'])/(result_df['max_intensity']+result_df['min_intensity'])
        total_result_df=pd.concat([total_result_df,result_df])

    return total_result_df


def prop_local_maximums(result_df,img,img_binary):
    
    coordinats=[]
    max_intensity=[]
    mean_intensity=[]
    min_intensity=[]
    
    for id in result_df.index[:20]:
        frame=total_result_df.loc[id,'frame']
        props=total_result_df.loc[id,:]

        min_row,min_col,max_row,max_col=props.loc["bbox-0":"bbox-3"].values.astype(int)
        region=img[min_row:max_row,min_col:max_col]
    
         
        max_intensity.append(np.max(region[img_binary==255]))
        mean_intensity.append(np.mean(region[img_binary=255]))
        min_intensity.append(np.min(np.mean(region[img_binary=255]))
    
       
        coordinates=peak_local_max(region, min_distance=5)
        coordinats.append(coordinats)
       
        
        ## viz
        plt.figure()
            plt.imshow(region)
        if (coordinates.size != 0.):
            plt.plot(coordinates[:, 1], coordinates[:, 0], 'r.')
        
    return coordinats,max_intensity,mean_intensity,min_intensity

def filter_objects(result_df,min_area,images):
    
    ##===================================##
    ## filter object less than 5px in area
    ##===================================##
    result_df=result_df[result_df['area']>min_area]


    ##================================##
    # filter objects close to border
    ##================================##
    result_df["boarder_flag"]=0
    row,col=images[frame].shape

    for i in result_df.index:
        t_row=int(result_df.loc[i]['centroid-0']-result_df.loc[i]['major_axis_length'])
        t_col=int(result_df.loc[i]['centroid-1']-result_df.loc[i]['major_axis_length'])

        if ((np.max([0,t_row])==0) or (np.min([row,t_row])==row)):
            result_df.loc[i,"boarder_flag"]=1.0
            #print(t_row,t_col)
            #print(total_result_df.loc[i,"boarder_flag"])
        
        if (np.max([0,t_col])==0) or (np.min([col,t_col]))==col:
            #print(t_row,t_col)
            result_df.loc[i,"boarder_flag"]=1.0
            #print(total_result_df.loc[i,"boarder_flag"])
        
    return result_df


# In[20]:


for file_prefix in ["43125_Dark_prepared"]:
    print(file_prefix)
    binary_images=images_dict[file_prefix]['binary']
    images=images_dict[file_prefix]['img']
    
    print(binary_images.shape)
    regions_props_df=props_measurement(binary_images,images)
    regions_props_df["file_prefix"]=file_prefix
    


# In[21]:


images.dtype


# In[24]:


regions_props_df


# ## Secondary objects

# In[ ]:


## local maximum
for id in total_result_df.index[:20]:
    frame=total_result_df.loc[id,'frame']
    props=total_result_df.loc[id,:]

    min_row,min_col,max_row,max_col=props.loc["bbox-0":"bbox-3"].values.astype(int)
    region=images_prepared[frame,min_row:max_row,min_col:max_col]
    
    plt.figure()
    plt.imshow(region)
    coordinates=peak_local_max(region, min_distance=5,threshold_abs=props.loc['min_intensity'])
    if (coordinates.size != 0.):
        plt.plot(coordinates[:, 1], coordinates[:, 0], 'r.')
    
    


# ## Viz results

# In[ ]:


def plot_one_entity(img,segmented,props):
    f = plt.figure(figsize=(10,3))
    ax_img = f.add_subplot(161)
    ax_region_ext = f.add_subplot(162)
    ax_region_ext_seg = f.add_subplot(163)
    ax_label = f.add_subplot(164)
    
    # ax_img
    ax_img.imshow(img,cmap="gray")
    min_row,min_col,max_row,max_col=props.loc["bbox-0":"bbox-3"].values.astype(int)
    rect =matplotlib.patches.Rectangle((min_col, min_row), max_col - min_col, max_row - min_row,fill=False, edgecolor='red', linewidth=3)
    ax_img.add_patch(rect)
    ax_img.axis("off")
    ax_img.set_title(str(props.loc["label"]))
    ax_img.axis("off")
 
    # ax_region_ext
    delta=7
    min_row_ext=np.max([0,min_row-delta])
    max_row_ext=max_row+delta
    min_col_ext=np.max([0,min_col-delta])
    max_col_ext=max_col+delta
    ax_region_ext.imshow(img[min_row_ext:max_row_ext,min_col_ext:max_col_ext],cmap="gray")
    ax_region_ext.axis("off")
    
    # ax_segment
    ax_region_ext_seg.imshow(segmented[min_row_ext:max_row_ext,min_col_ext:max_col_ext],cmap="gray")
    ax_region_ext_seg.axis("off")
    
    #attributes
    attributes_intenisty="frame={:.2f}    mean_intensity={:.2f}   contrast={:.2f} \n".format(props['frame'],props['mean_intensity'],props["contrast"])
    attributes_geometry="minor={:.2f}   major={:.2f}  \n".format(props["minor_axis_length"],props["major_axis_length"])
    attributes_shape="area={:.2f}   eccentricity={:.2f}   \n".format(props["area"],props["eccentricity"])
    attributes=attributes_intenisty+attributes_geometry+attributes_shape                                                                                                
    ax_label.text(0,0.5,attributes)
    ax_label.axis("off")
    
    
    #plt.tight_layout()


# In[ ]:


Selection=total_result_df[total_result_df["area"]>100]

for id in Selection.index[:5]:
    frame=Selection.loc[id,'frame']
    plot_one_entity(images_prepared[frame],images_binary[frame],Selection.loc[id,:])
   

