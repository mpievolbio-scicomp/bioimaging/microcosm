#!/usr/bin/env python
# coding: utf-8

# In[2]:


from ipywidgets import IntSlider
from ipywidgets.embed import embed_minimal_html

slider = IntSlider(value=40)
embed_minimal_html('../results/export.html', views=[slider], title='Widgets export')


# In[ ]:




