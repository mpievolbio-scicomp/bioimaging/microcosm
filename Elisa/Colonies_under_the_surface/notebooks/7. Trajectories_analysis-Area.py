#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns

import skimage.io as io
from skimage import util

from glob import glob
import trackpy

import ast
from ipywidgets import interact, widgets


# import code from the src
### import code from src folder
import sys
sys.path.append('../src/')
import chow_test_v
import area_analysis

import warnings
warnings.filterwarnings('ignore')

from sklearn.metrics import r2_score, median_absolute_error, mean_absolute_error
from sklearn.metrics import median_absolute_error, mean_squared_error, mean_squared_log_error


# ## Read data

# In[2]:


file_list=glob("../data/*_Dark_prepared.tif")
images_dict={}
for file_name in file_list:
    images_prepared=io.imread(file_name)
    file_prefix=os.path.split(file_name)[1].split('.')[0].split("_prepared")[0]
    
    binary_file=file_name.split(".tif")[0]+'_segmented'+'.tif'
    images_binary=io.imread(binary_file)
    tracks=pd.read_csv("../data/tracks_overlay_id_"+file_prefix+'.csv')
    tracks_big=pd.read_csv("../data/tracks_overlay_id_"+file_prefix+'_big.csv')
    img_dict={"img":images_prepared,"binary":images_binary,"tracks":tracks,"tracks_big":tracks_big}
    
    images_dict[file_prefix]=img_dict


# ## Area
# 
# * The objects have dynamic behavior. The area measurements are fluctuate with a time. My try to divide objects on subunits does not work  (geometry method, local-maximum method) . 
# * Area are generally grow in two ways: linear growth of the object itself (linearly?) and aggregation.
# 

# In[3]:


Area_replicas=pd.DataFrame()

for file_prefix in images_dict.keys():
    tracks=images_dict[file_prefix]["tracks_big"]
    images=images_dict[file_prefix]["img"]
    binary=images_dict[file_prefix]["binary"]

    # set up grid for plt plot
    sns.set(font_scale=2)
    Ngroups=tracks.groupby('particle').ngroups
    Nrows=np.int(np.ceil(Ngroups/5))
    fig,ax=plt.subplots(nrows=Nrows,ncols=5,figsize=(40,Nrows*5))
    fig.suptitle(file_prefix)
    axx=ax.ravel()

    
    #analyse each trajectory separately
    # 1) anomaly detection
    # 2) chow test anomalies
    # 3) fit according anomalies detection
    counter=0
    Area_file_prefix=pd.DataFrame()
    
    for id, traj in tracks.groupby('particle'):
        df_result=area_analysis.area_analysis(traj,axx[counter])
        Area_file_prefix=pd.concat([Area_file_prefix,df_result])
        
        if df_result.shape[0]>1:
            handles, labels = axx[counter].get_legend_handles_labels()
        counter=counter+1
   
    fig.legend(handles, labels, loc='upper center')
    Area_file_prefix["file_prefix"]=file_prefix
    fig.savefig("../results/Area_updated_fit__{}.png".format(file_prefix))
    
    Area_replicas=pd.concat([Area_replicas,Area_file_prefix])
    


# In[12]:


fig,ax = plt.subplots(nrows=1,ncols=3)
p1, = ax[0].plot([1,2,3], label="line 1")
p2, = ax[1].plot([3,2,1], label="line 2")
p3, = ax[2].plot([2,3,1], label="line 3")

handles, labels = ax[0].get_legend_handles_labels()

# # reverse the order
# ax.legend(handles[::-1], labels[::-1])

# # or sort them by labels
# import operator
# hl = sorted(zip(handles, labels),
#             key=operator.itemgetter(1))
# handles2, labels2 = zip(*hl)

ax[0].legend(handles2, labels2)
ax[1].legend(handles2, labels2)
ax[2].legend(handles2, labels2)
fig.legend(handles, labels, loc='upper center')


# In[83]:


file_prefix="43121_Dark"
tracks=images_dict[file_prefix]["tracks_big"]
images=images_dict[file_prefix]["img"]
binary=images_dict[file_prefix]["binary"]


Ngroups=tracks.groupby('particle').ngroups
Nrows=np.int(np.ceil(Ngroups/5))

sns.set(font_scale=2)
fig,ax=plt.subplots(nrows=Nrows,ncols=5,figsize=(40,Nrows*5))
axx=ax.ravel()

counter=0
Area=pd.DataFrame()
for id, traj in tracks.groupby('particle'):
    df_result=area_analysis.area_analysis(traj,axx[counter])
    #print(df_result)
    Area=pd.concat([Area,df_result])
    counter=counter+1
Area=Area.reset_index()


# In[65]:


Area


# In[66]:


Area[Area["R2"]<0.20]


# In[74]:


ar=Area[Area["len"]>19]


# In[75]:


plt.scatter(np.log10(ar["MSE"]),ar["len"])
plt.show()
plt.scatter(np.log10(ar["intersept"]),ar["slope"])
plt.show()
plt.scatter(np.log10(ar["intersept"]),ar["len"])


# In[76]:


plt.hist(ar["slope"], bins = 20)

np.quantile(ar["slope"], [0.1, 0.5, 0.9])


# In[25]:


traj=tracks[tracks['particle']==4]
window=5
scale=1.96


fig,ax=plt.subplots(figsize=(15,5))
anomalies=area_analysis.simple_anomaly_detector(traj,ax,5,1.96,plot_flag=True)
print(anomalies)
result=area_analysis.fit_area_with_anomalies(traj,anomalies,ax)


# In[ ]:


# select=['slope_area', 'intersept_area','R2_area','traj_len', 'max_relative_diff_area', 'time_relative_max_diff_are']
# sns.pairplot(data=Area[select])

