import numpy as np
import pandas as pd

def displacement_per_frame(Traj):
    X=Traj['c'].values
    Y=Traj['r'].values
    dist_x=(X[1:]-X[:-1])
    dist_y=(Y[1:]-Y[:-1])
    dist=Traj["dist"].values[1:]
    delta_Area=((Traj["area"].values[1:]-Traj["area"].values[:-1]))
    frames=Traj['frame'].values[1:]
    traj_id=Traj['traj_id'][1:].values
    
    Result=np.column_stack([traj_id,frames,dist,dist_x,dist_y,delta_Area])
    Result=pd.DataFrame(Result,columns=['traj_id','frame','dist','dist_x','dist_y','delta_Area'])
    return Result
    