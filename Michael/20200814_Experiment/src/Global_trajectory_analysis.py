import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

def plot_traj_from_Start(Traj,nr,ax):
    """
    plot one trajectory starting from the zero
    input: coordinats of the trajectory, nr number of rows, ax plot axis
    output: end to end distance
    """
    
    X=Traj['c'].values
    Y=nr-Traj['r'].values
    X=X-X[0]
    Y=Y-Y[0]
    ax.plot(X,Y,lw=3,color="red",alpha=0.3)



def LinearRegression_area_vs_time(OneTraj,dx,dt):
    
    """
    fit linear regression to the area vs time for selected trajectory
    input: one trajectory coordinats, 
            dx px in physical units 
            dt in physical units
    output: 
    """
    traj_id=OneTraj["traj_id"].values[0]
    
    len_traj=OneTraj['frame'].size
    frames=(OneTraj['frame'].values-OneTraj['frame'].min())*dt # start from time=0, it is required for fitting
    Time=frames.reshape((-1, 1))
    Area=OneTraj['area'].values*dx*dx
    std_area=np.std(Area)

    model=LinearRegression()
    model.fit(Time, Area)
    area_predict=model.predict(Time)
    rmse=np.sqrt(mean_squared_error(Area))
    
    #ID=OneTraj['traj_id'].values[0]
    FitResult=pd.Series([traj_id,model.coef_[0],model.intercept_,model.score(Time, Area),rmse,std_area,len_traj],
                        index=["traj_id","slope","intersept","R2","rmse","std_area","len_traj"])
    
    #print(model.score(Time, Area),model.coef_[0],model.intercept_)
    #plt.plot(Time,Area,color="blue")
    #plt.plot(Time,model.predict(Time),color="red")
    
    return FitResult


def traj_analysis(Traj,dx,dt,nr):
    
    """
    calculates traj feature-descriptors
    input: one trajectory coordinats, 
            physical units px size in mkm
            time in s
    output: Frame_length, End to end distance; distance along path; Efficiency,Box_dx;Box_dy;dist_dx;dist_dy;
            meanSpeed; stdSpeed; meanVx; stdVx; meanVy; stdVy 
    """
    # sort to the time order
    Traj=Traj.sort_values('frame')
    X=Traj['c'].values
    Y=nr-Traj['r'].values
    
    # Traj length
    length=Traj.shape[0]
    
    #####################################################
    # Location
    meanX=X.mean()
    meanY=Y.mean()
    
    #####################################################
    # Efficiency
    # along X
    EtE_x=np.abs((X[-1]-X[0]))
    AP_x=np.sum(np.abs(X[1:]-X[0:-1]))
    eff_x=EtE_x/AP_x if(AP_x>0) else 'NA'
    
    # along Y
    EtE_y=np.abs((Y[-1]-Y[0]))
    AP_y=np.sum(np.abs(Y[1:]-Y[0:-1]))
    eff_y=EtE_y/AP_y if(AP_y>0) else 'NA'
                 
    #direction
    dy=Y[-1]-Y[0]
    dx=X[-1]-X[0]
    Directions=np.arctan2(dy,dx)* 180 / np.pi
                 
    # general
    EtE_distance=np.sqrt(((X[-1]-X[0])**2+(Y[-1]-Y[0])**2))*dx
    AP_distance=np.sum(np.sqrt((((X[1:]-X[0:-1])**2+(Y[1:]-Y[0:-1])**2))))*dx
    eff=EtE_distance/AP_distance if(AP_distance>0) else 'NA'
    #print(EtE_distance,AP_distance)             
    
                 
    #####################################################    
    # Speed
    # Speed directional speed
    Speed=np.sqrt((((X[1:]-X[0:-1])**2+(Y[1:]-Y[0:-1])**2)))*dx/dt
    stdSpeed=np.std(Speed)
    medSpeed=np.median(Speed)
    
    medSpeed_dx=np.median(X[1:]-X[0:-1])*dx/dt
    stdSpeed_dx=np.std(X[1:]-X[0:-1])*dx/dt
    
    medSpeed_dy=np.median(Y[1:]-Y[0:-1])*dx/dt
    stdSpeed_dy=np.std(Y[1:]-Y[0:-1])*dx/dt
    
    # Area
    medArea=np.median(Traj['area'].values)
    stdArea=np.std(Traj['area'].values)
    result=pd.Series([length,meanX,meanY,EtE_x,AP_x,eff_x,EtE_y,AP_y,eff_y,EtE_distance,AP_distance,eff,Directions,medSpeed,stdSpeed,
                                                     medSpeed_dx,stdSpeed_dx,medSpeed_dy,stdSpeed_dy,medArea,stdArea],index=
                    ['len_traj','meanX','meanY','EtE_x','AP_x','eff_x','EtE_y','AP_y','eff_y','EtE_distance','AP_distance','eff','Directions','medSpeed','stdSpeed',
                                                     'medSpeed_dx','stdSpeed_dx','medSpeed_dy','stdSpeed_dy','medArea','stdArea'])

    return(result)

                 
                 