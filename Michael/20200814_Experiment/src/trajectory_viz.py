import numpy as np
import pandas as pd
import skimage.io as io
import matplotlib.pyplot as plt

def plot_traj(Traj,ax):
    ax.plot(np.ceil(Traj['c']),np.ceil(Traj['r']),label="ID={:}".format(Traj['traj_id'].values[0]),lw=3)
    ax.legend()
    

def traj_on_image(Traj,segmented,ax):
    max_c,min_c=int(Traj['c'].max()),int(Traj['c'].min())
    max_r,min_r=int(Traj['r'].max()),int(Traj['r'].min())
    segmeted_traj=segmented[:,min_r:max_r,min_c:max_c].sum(axis=0)
    ax.imshow(segmeted_traj)
    ax.plot(Traj['c']-min_c,Traj['r']-min_r,lw=3,label=str(Traj["traj_id"][0]))
    ax.legend()
    print(max_c,min_c,max_r,min_r)