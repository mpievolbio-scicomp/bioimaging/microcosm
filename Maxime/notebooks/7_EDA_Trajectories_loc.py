#!/usr/bin/env python
# coding: utf-8

# In[2]:


import skimage.io as io
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

from glob import glob
import os
import sys


sys.path.append('../src/')
import trajectory_viz
from getfile_name import get_file_name
from Local_trajectory_analysis_loc import displacement_per_frame


# In[3]:


dx=11  #mkm/px
dt=10 #min


# In[4]:


INPUT="../data"
images_segmented=io.imread(get_file_name(INPUT,"segmented"))
nr,nc=images_segmented[0,:,:].shape

File_traj=get_file_name(INPUT,"Traj_data_cleaned_3px_min_area")
df_Traj=pd.read_csv(File_traj)


# In[5]:


df_Traj.columns


# ## First Derivative across time
# Calculate per frame d,dx,dy,d_area

# In[6]:


df_delta=pd.DataFrame(columns=["traj_id","dist","dist_x","dist_y","delta_Area"])
for name,group in df_Traj.groupby("traj_id"):
    delta=displacement_per_frame(group)
    #print(delta.shape)
    df_delta=pd.concat([df_delta,delta])    


# In[7]:


plt.hist(df_delta["dist"]*dx,bins=100);
plt.title("Per 10 min frame displacement")
plt.xlabel("displacement mkm")
plt.ylabel("counts")
plt.savefig("../results/for presentation/Per_frame_displacement"+".png",bbox_inches='tight')


# In[21]:


np.sqrt(0.5*600)


# In[5]:


df_delta.describe()


# ## Cleaning outliers

# In[6]:


#Outliers=df_delta.quantile(0.95)
#Outliers.drop(["traj_id","frame","dist_x","dist_y"],inplace=True)


#for (name,k) in zip(Outliers.index,Outliers):
    #print(name,k)
    #df_delta=df_delta[df_delta[name]<k] 
    
#SelectedTraj=df_delta.traj_id.unique()
#df_delta.shape
#SelectedTraj=df_delta.traj_id.unique()


# ## Correlation dx,dy

# In[39]:


XX=df_delta["dist_x"]*dx
YY=df_delta["dist_y"]*dx
plot=sns.jointplot(x=XX,y=YY,kind='kde')
plot.ax_marg_x.set_xlim(-10,10)
plot.ax_marg_y.set_ylim(-10,20)
plot.ax_joint.axvline(x=0,linestyle='--',color='red')
plot.ax_joint.axhline(y=0,linestyle='--',color='red')
plot.savefig("../results/for presentation/dx_dy_plot"+".png",bbox_inches='tight')


# ## Dist delta_Area

# In[40]:


plot=sns.jointplot(x=df_delta["dist"],y=df_delta["delta_Area"],kind='kde')
#plot.ax_marg_x.set_xlim(0,2)
#plot.ax_marg_y.set_ylim(0,20)


# ## Correlation frame vs delta_Area

# In[9]:


fig, ax = plt.subplots(figsize=(15,7))
plot=sns.boxplot(x="frame", y="delta_Area", data=df_delta)

#plot.ax_marg_x.set_xlim(0,26)
#plot.ax_marg_y.set_ylim(-20,20)


# ## Correlation frame vs dist

# In[16]:


sns.set(font_scale=1.5)
fig, ax = plt.subplots(figsize=(25,7))
df_delta["hours"]=np.round((df_delta["frame"]+62)*10/60,decimals=2)
plot=sns.boxplot(x="hours", y="dist", data=df_delta)

#plot.ax_marg_x.set_xlim(0,29)
#plot.ax_marg_y.set_ylim(0, 50)
#plot.set_ylim(0, 50)
plt.savefig("../results/for presentation/frame_dist_plot"+".png",bbox_inches='tight')

