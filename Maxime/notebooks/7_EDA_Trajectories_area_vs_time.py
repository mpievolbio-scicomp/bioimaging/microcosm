#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
from skimage import util
import seaborn as sns
from glob import glob

import os
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

import sys
sys.path.append('../src/')
from Global_trajectory_analysis import LinearRegression_area_vs_time
from trajectory_viz import plot_traj


# ### Physical units

# In[2]:


dx=11 # mkm
dt=10 # min


# ## Read data

# In[3]:


INPUT="../data"
FileName="../data/supplementaryMovie5_ancestor_ALI_Traj_data_cleaned_3px_min_area.csv"
Traj=pd.read_csv(FileName)
Traj.drop(columns=['Unnamed: 0', 'Unnamed: 0.1'],inplace=True)
TrajGroup=Traj.groupby('traj_id')
print("Total number points {:}".format(Traj.shape))
print("Number Trajectories {:}".format(TrajGroup.ngroups))


# In[4]:


Traj.columns


# ## Select only long trajectories

# In[5]:


TrajLong=TrajGroup.filter(lambda x:len(x['frame'])>25)
TrajLongGroup=TrajLong.groupby('traj_id')


# In[6]:


TrajMeanArea=TrajLong.groupby('traj_id').apply(lambda x: np.mean(x['area']))


# In[7]:


plt.hist(TrajMeanArea*dx*dx,bins=50);
plt.title("Mean area across the trajectory")
plt.xlabel("area mkm^2")
plt.ylabel("counts")
plt.savefig("../results/for presentation/Mean area across the trajectory"+".png",bbox_inches='tight')


# In[8]:


Area=TrajMeanArea*dx*dx


# In[9]:


Area.describe()


# ## Linear regression fit area vs time

# In[10]:


def LinearRegression_area_vs_time(OneTraj,dx,dt):
    
    """
    fit linear regression to the area vs time for selected trajectory
    input: one trajectory coordinats, 
            dx px in physical units 
            dt in physical units
    output: 
    """
    trajID=OneTraj["traj_id"].values[0]
    len_traj=OneTraj['frame'].size
    # start from time=0, it is required for fitting
    frames=(OneTraj['frame'].values-OneTraj['frame'].min())*dt 
    Time=frames.reshape((-1, 1))
    Area=OneTraj['area'].values*dx*dx
    std_area=np.std(Area)

    model=LinearRegression()
    model.fit(Time, Area)
    area_predict=model.predict(Time)
    rmse=np.sqrt(mean_squared_error(Area,area_predict))
    
    FitResult=pd.Series([trajID,model.coef_[0],model.intercept_,model.score(Time, Area),rmse,np.median(Area),std_area,len_traj],
                        index=["traj_id","slope","intersept","R2","rmse","med_area","std_area","len_traj"])
    #print(model.score(Time, Area),model.coef_[0],model.intercept_)
    #plt.plot(Time,Area,color="blue")
    #plt.plot(Time,model.predict(Time),color="red")
    
    return FitResult


# In[11]:


RegFitArea=TrajLongGroup.apply(LinearRegression_area_vs_time,dx,dt)
RegFitArea.head(5)


# In[18]:


Sample_df=RegFitArea.sample(n=14)
SelectedId=Sample_df["traj_id"]
sns.set(font_scale=0.9)

fig,ax=plt.subplots(nrows=2,ncols=7,figsize=(15,8))
counter=0
for id,axplot in zip(SelectedId,ax.ravel()):
    Traj=TrajGroup.get_group(id)
    #plot_traj(Traj,axplot)
    start=Traj.frame.iloc[0]
    axplot.plot((Traj.frame-start)*dt,Traj.area*dx*dx)
    max_frame=Traj.frame.max()-start
    axplot.plot([0,max_frame*dt],[RegFitArea.loc[id].intersept,RegFitArea.loc[id].intersept+RegFitArea.loc[id].slope*Traj.frame.max()],
                label="slope={:.2f} \n R2={:.2f} \n rmse={:.2f}".format(RegFitArea.loc[id].slope,RegFitArea.loc[id].R2,RegFitArea.loc[id].rmse))
    counter=counter+1
    axplot.legend()
fig.savefig("../results/for presentation/Sample_fit"+".png",bbox_inches='tight')


# In[13]:


RegFitArea.slope.hist(bins=50)
plt.title("Slope fit: y = Ax+B")
plt.xlabel("slope [mkm^2/min]")
plt.ylabel("counts")
plt.savefig("../results/for presentation/Slope_fit"+".png",bbox_inches='tight')


# In[14]:


20*11*11


# In[18]:


RegFitArea.columns


# In[39]:


plt.scatter(RegFitArea.rmse,RegFitArea.slope,alpha=0.1)
plt.title("RMSE vs slope")
plt.xlabel("RMSE")
plt.ylabel("Slope")
plt.hlines(y=0,xmin=0,xmax=RegFitArea.rmse.max(),linestyle='--',color='red')
plt.savefig("../results/for presentation/Slope_RMSE"+".png",bbox_inches='tight')


# In[11]:


RegFitArea[RegFitArea.len_traj>25].slope.hist(bins=100)


# In[12]:


RegFitArea[RegFitArea.len_traj>25].slope.describe()


# In[20]:


Sample_df=RegFitArea[RegFitArea.len_traj>25].sample(n=14)
SelectedId=Sample_df["traj_id"]
sns.set(font_scale=0.9)

fig,ax=plt.subplots(nrows=2,ncols=7,figsize=(20,8))
counter=0
for id,axplot in zip(SelectedId,ax.ravel()):
    Traj=TrajGroup.get_group(id)
    #plot_traj(Traj,axplot)
    start=Traj.frame.iloc[0]
    axplot.plot((Traj.frame-start)*dt,Traj.area*dx*dx)
    max_frame=Traj.frame.max()
    axplot.plot([0,max_frame*dt],[RegFitArea.loc[id].intersept,RegFitArea.loc[id].intersept+RegFitArea.loc[id].slope*Traj.frame.max()],
                label="slope={:.2f} \n R2={:.2f} \n rmse={:.2f}".format(RegFitArea.loc[id].slope,RegFitArea.loc[id].R2,RegFitArea.loc[id].rmse))
    counter=counter+1
    axplot.legend()
plt.savefig("../results/for presentation/Sample_area_fit"+".png",bbox_inches='tight')


# In[35]:


RegFitArea.describe()


# In[9]:


sns.set(font_scale=2)
RegFitArea[["slope","intersept","R2","std_area","len_traj"]].hist(bins=100,layout=(1,5),figsize=(28,7));


# In[10]:


sns.pairplot(RegFitArea[["slope","intersept","R2","std_area","len_traj"]])


# In[12]:


plot=sns.jointplot(x=RegFitArea["slope"],y=RegFitArea["R2"],kind='kde')
#plot.ax_marg_x.set_xlim(-5,5)

plot=sns.jointplot(x=RegFitArea["slope"],y=RegFitArea["std_area"],kind='kde')
#plot.ax_marg_x.set_xlim(-5,5)
#plot.ax_marg_y.set_ylim(0,15)

