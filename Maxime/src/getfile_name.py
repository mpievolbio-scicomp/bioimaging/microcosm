from glob import glob
import os

def get_file_name(INPUT,pf):
    pf="*"+pf+"*"
    FileName=glob(os.path.join(INPUT,pf))
    
    if len(FileName)>1:
        print("Error: to many files")
        print(FileName)
    print(FileName[0])
    return FileName[0]