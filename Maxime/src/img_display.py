import glob

import os
import glob

import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import cv2 

import skimage.io
import pandas as pd
import string

from skimage.transform import hough_circle, hough_circle_peaks
from skimage.draw import circle,circle_perimeter
from skimage import data, color
from skimage import feature
import seaborn as sns

from skimage import io, exposure

from itertools import product

import sys


# code source https://scikit-image.org/docs/dev/auto_examples/applications/plot_3d_image_processing.html#sphx-glr-auto-examples-applications-plot-3d-image-processing-py
def display(im3d, save_fig,step):
    fig, axes = plt.subplots(nrows=5, ncols=5, figsize=(16, 14))

    vmin = im3d.min()
    vmax = im3d.max()
    hour=0
    
    for ax, image in zip(axes.flatten(), im3d[::step]):
        ax.imshow(image, cmap="gray", vmin=vmin, vmax=vmax)
        ax.set_xticks([])
        ax.set_title(str(hour)+" hour")
        ax.set_yticks([])
        frame=hour+step
        hour=hour+1
    fig.savefig("../results/for_presentation/"+save_fig,bbox_inch='tight')

def display_hist(im3d, save_fig, step):
    fig, axes = plt.subplots(nrows=5, ncols=5, figsize=(16, 14))
    hour=0
    
    for ax, image in zip(axes.flatten(), im3d[::step]):
        ax.hist(image.ravel(),bins=256)
        ax.set_title(str(hour)+" hour")
        ax.set_ylim([0,300000])
        frame=hour+step
        ax.ticklabel_format(axis="y", style="scientific", scilimits=(0, 0))
        hour=hour+1
         
    fig.savefig("../results/for_presentation/"+save_fig) 
        
def show_plane(ax, plane, cmap="gray", title=None):
    ax.imshow(plane, cmap=cmap)
    ax.axis("off")

    if title:
        ax.set_title(title)
        
def plot_hist(ax, data, title=None):
    # Helper function for plotting histograms
    ax.hist(data.ravel(), bins=256)
    ax.ticklabel_format(axis="y", style="scientific", scilimits=(0, 0))

    if title:
        ax.set_title(title)

def Plot_layer_time(img,Layer,CenterY,Step,plot_label):
    """
    cut and display  defined layer across time frames
    
    input: 
        img:     image
        Layer:   hight of the layer in px
        CenterY: y coordinats of the center of the layer
        Step:    number frames per hour
        plot_label: str title of the plot
    
    """
    NumFrames=img.shape[0]
    
    
    TimeFrame=range(0,NumFrames,Step)
    NumLayers=len(TimeFrame)
    
    ImgLayerStack=np.zeros([NumLayers*Layer,img.shape[2]])
    Interval=np.array([0,Layer])
    
    plt.figure(figsize=(20,20))
    cmap_fig=plt.get_cmap('plasma')
    
    
    for i in TimeFrame:
        cut=img[i,CenterY-Layer/2:CenterY+Layer/2,:].copy()
        ImgLayerStack[Interval[0]:Interval[1],:]=cut
        #exposure.equalize_adapthist(cut,clip_limit=0.03)
        Interval=Interval+Layer
    
    plt.figure(figsize=(20,20))
    plt.imshow(ImgLayerStack,cmap=plt.cm.gray)
    
    # put hour label to the plot
    count=1
    text_y_coord=range(Layer/2,NumLayers*Layer,Layer)
    for y in text_y_coord:
        t=str(count)+' hour'
        plt.text(img.shape[2]+100, y,t,fontsize=12)
        count=count+1
    
    plt.title(plot_label)
    plt.axis('off')
    #plt.savefig("../../results/"+plot_label+".png",bbox_inches='tight')
    
