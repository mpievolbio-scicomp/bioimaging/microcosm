import os
import glob

import os
import glob

import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import cv2 

import skimage.io
import pandas as pd
import string

from skimage.transform import hough_circle, hough_circle_peaks
from skimage.draw import circle,circle_perimeter
from skimage import data, color
from skimage import feature
import seaborn as sns

from skimage import io, exposure

from itertools import product

import sys

# exclude warnings
import warnings
warnings.filterwarnings('ignore')

def Overall_intenstity(images):
    """
    calculate median intensity across 
    the frame
    
    input: grey scale tiff img
    output: data frame
    """
    
    num_frame=images.shape[0]
    output_list=[]
    for i in range(num_frame):
        output_list.append({"frame":i,"img_median":np.median(images[i,:,:])})

    Overall_median=pd.DataFrame(output_list)
    return(Overall_median)
           
def Translate_coord_polar(img,CenterX,CenterY):
    """       
    Translate Coordinats to the polar axis
    idea to get indices from source https://stackoverflow.com/questions/46134827/
    how-to-recover-original-indices-for-a-flattened-numpy-array
    
    input: img tiff stack
    output: data frame
    """
 
    NumFrames=img.shape[0]
    X,Y = img[0,:,:].shape
    X_, Y_= zip(*product(range(X), range(Y)))

    # conver to image coordinates [0,0] is counted from top left corner
    dX=np.array(img[0,:,:].shape[0])
    X_=np.abs(X_-dX)

    # in image row coordinats correspond to the Y
    YY=X_-CenterY
    XX=Y_-CenterX
    Rho = np.sqrt(XX**2+YY**2)
    Phi = np.arctan2(YY,XX)

    DataPolar=pd.DataFrame({"X":XX,"Y":YY,"Rho":Rho,"Phi":Phi})
    
    # add the intesity for each coordinats as additional column
    for frame in range(0,NumFrames,1):
        colname=str(frame+1)+"_frame"
        DataPolar[colname] = img[frame,:,:].flatten()
    
    return DataPolar

def Radial_statistics(DataPolar,nlayers,Radius,Step,NumFrames):
    """       
    Calculate intensity statistics per dr layer
    
    input: DataPolar,
           dr -layer size in px
           Radius,
           Step 
    output: data frame
    """
    # layers setup
    #nlayers=Radius/dr-1
    dr=Radius/(nlayers+1)
    print("dr="+str(dr)+"px","nlayers="+str(nlayers),"Radius="+str(Radius)+"px")
    r  = np.linspace(0,Radius-dr,num=nlayers)
    print(r)

    # columns with intensity data used later as a cycle iterator


    radial=[]
    general_growth=[]

    for col in range(4,NumFrames,Step):
        cl_name=DataPolar.columns[col]
        frame=cl_name.split('_')[0]
        general_growth.append([frame,DataPolar.loc[:,cl_name].median()])
    
        for rr in r:
            select_int=DataPolar.loc[(DataPolar.Rho >= rr) & (DataPolar.Rho < rr+dr),cl_name]
            median = np.median(select_int)
            upper_quartile = np.percentile(select_int, 75)
            lower_quartile = np.percentile(select_int, 25)
            iqr = upper_quartile - lower_quartile
            upper_whisker = upper_quartile+1.5*iqr
            lower_whisker = lower_quartile-1.5*iqr
            output=pd.Series([rr,median,upper_quartile,lower_quartile,upper_whisker,lower_whisker,frame],
                             index=["r","mmedian","upper_quartile","lower_quartile","upper_whisker","lower_whisker","time_frame"])
            radial.append(output)
    
    Radial_statistics=pd.DataFrame(radial,columns=["r","mmedian","upper_quartile","lower_quartile","upper_whisker","lower_whisker","time_frame"])
    
    return Radial_statistics



